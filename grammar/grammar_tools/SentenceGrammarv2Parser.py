# Generated from SentenceGrammarv2.g4 by ANTLR 4.7.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\7")
        buf.write("=\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\3\2")
        buf.write("\3\2\3\2\3\2\3\2\3\2\3\2\3\2\5\2\27\n\2\3\3\3\3\3\3\3")
        buf.write("\3\3\3\3\3\5\3\37\n\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4")
        buf.write("\3\4\5\4*\n\4\3\5\3\5\3\5\3\6\3\6\3\6\3\7\3\7\3\7\3\7")
        buf.write("\3\7\3\7\3\7\3\7\3\7\5\7;\n\7\3\7\2\2\b\2\4\6\b\n\f\2")
        buf.write("\2\2C\2\26\3\2\2\2\4\36\3\2\2\2\6)\3\2\2\2\b+\3\2\2\2")
        buf.write("\n.\3\2\2\2\f:\3\2\2\2\16\17\7\5\2\2\17\27\5\4\3\2\20")
        buf.write("\21\7\4\2\2\21\27\5\b\5\2\22\23\7\6\2\2\23\27\5\6\4\2")
        buf.write("\24\25\7\3\2\2\25\27\5\f\7\2\26\16\3\2\2\2\26\20\3\2\2")
        buf.write("\2\26\22\3\2\2\2\26\24\3\2\2\2\27\3\3\2\2\2\30\31\7\3")
        buf.write("\2\2\31\37\5\f\7\2\32\33\7\6\2\2\33\37\5\6\4\2\34\35\7")
        buf.write("\4\2\2\35\37\5\n\6\2\36\30\3\2\2\2\36\32\3\2\2\2\36\34")
        buf.write("\3\2\2\2\37\5\3\2\2\2 !\7\3\2\2!*\5\f\7\2\"#\7\6\2\2#")
        buf.write("*\5\6\4\2$%\7\5\2\2%*\5\4\3\2&\'\7\4\2\2\'*\5\n\6\2(*")
        buf.write("\3\2\2\2) \3\2\2\2)\"\3\2\2\2)$\3\2\2\2)&\3\2\2\2)(\3")
        buf.write("\2\2\2*\7\3\2\2\2+,\7\3\2\2,-\5\f\7\2-\t\3\2\2\2./\7\3")
        buf.write("\2\2/\60\5\f\7\2\60\13\3\2\2\2\61\62\7\5\2\2\62;\5\4\3")
        buf.write("\2\63\64\7\4\2\2\64;\5\b\5\2\65\66\7\6\2\2\66;\5\6\4\2")
        buf.write("\678\7\3\2\28;\5\f\7\29;\3\2\2\2:\61\3\2\2\2:\63\3\2\2")
        buf.write("\2:\65\3\2\2\2:\67\3\2\2\2:9\3\2\2\2;\r\3\2\2\2\6\26\36")
        buf.write("):")
        return buf.getvalue()


class SentenceGrammarv2Parser ( Parser ):

    grammarFileName = "SentenceGrammarv2.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [  ]

    symbolicNames = [ "<INVALID>", "SEQ", "NONTARG", "TARG", "GENE", "WS" ]

    RULE_e = 0
    RULE_et = 1
    RULE_eg = 2
    RULE_es = 3
    RULE_ent = 4
    RULE_esp = 5

    ruleNames =  [ "e", "et", "eg", "es", "ent", "esp" ]

    EOF = Token.EOF
    SEQ=1
    NONTARG=2
    TARG=3
    GENE=4
    WS=5

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class EContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return SentenceGrammarv2Parser.RULE_e

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class FirstGeneContext(EContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SentenceGrammarv2Parser.EContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def GENE(self):
            return self.getToken(SentenceGrammarv2Parser.GENE, 0)
        def eg(self):
            return self.getTypedRuleContext(SentenceGrammarv2Parser.EgContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFirstGene" ):
                return visitor.visitFirstGene(self)
            else:
                return visitor.visitChildren(self)


    class FirstSeqContext(EContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SentenceGrammarv2Parser.EContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def SEQ(self):
            return self.getToken(SentenceGrammarv2Parser.SEQ, 0)
        def esp(self):
            return self.getTypedRuleContext(SentenceGrammarv2Parser.EspContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFirstSeq" ):
                return visitor.visitFirstSeq(self)
            else:
                return visitor.visitChildren(self)


    class FirstNonTargContext(EContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SentenceGrammarv2Parser.EContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def NONTARG(self):
            return self.getToken(SentenceGrammarv2Parser.NONTARG, 0)
        def es(self):
            return self.getTypedRuleContext(SentenceGrammarv2Parser.EsContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFirstNonTarg" ):
                return visitor.visitFirstNonTarg(self)
            else:
                return visitor.visitChildren(self)


    class FirstTargContext(EContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SentenceGrammarv2Parser.EContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def TARG(self):
            return self.getToken(SentenceGrammarv2Parser.TARG, 0)
        def et(self):
            return self.getTypedRuleContext(SentenceGrammarv2Parser.EtContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFirstTarg" ):
                return visitor.visitFirstTarg(self)
            else:
                return visitor.visitChildren(self)



    def e(self):

        localctx = SentenceGrammarv2Parser.EContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_e)
        try:
            self.state = 20
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [SentenceGrammarv2Parser.TARG]:
                localctx = SentenceGrammarv2Parser.FirstTargContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 12
                self.match(SentenceGrammarv2Parser.TARG)
                self.state = 13
                self.et()
                pass
            elif token in [SentenceGrammarv2Parser.NONTARG]:
                localctx = SentenceGrammarv2Parser.FirstNonTargContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 14
                self.match(SentenceGrammarv2Parser.NONTARG)
                self.state = 15
                self.es()
                pass
            elif token in [SentenceGrammarv2Parser.GENE]:
                localctx = SentenceGrammarv2Parser.FirstGeneContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 16
                self.match(SentenceGrammarv2Parser.GENE)
                self.state = 17
                self.eg()
                pass
            elif token in [SentenceGrammarv2Parser.SEQ]:
                localctx = SentenceGrammarv2Parser.FirstSeqContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 18
                self.match(SentenceGrammarv2Parser.SEQ)
                self.state = 19
                self.esp()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class EtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return SentenceGrammarv2Parser.RULE_et

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class TargToGeneContext(EtContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SentenceGrammarv2Parser.EtContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def GENE(self):
            return self.getToken(SentenceGrammarv2Parser.GENE, 0)
        def eg(self):
            return self.getTypedRuleContext(SentenceGrammarv2Parser.EgContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTargToGene" ):
                return visitor.visitTargToGene(self)
            else:
                return visitor.visitChildren(self)


    class TargToSeqContext(EtContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SentenceGrammarv2Parser.EtContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def SEQ(self):
            return self.getToken(SentenceGrammarv2Parser.SEQ, 0)
        def esp(self):
            return self.getTypedRuleContext(SentenceGrammarv2Parser.EspContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTargToSeq" ):
                return visitor.visitTargToSeq(self)
            else:
                return visitor.visitChildren(self)


    class TargToNonTargContext(EtContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SentenceGrammarv2Parser.EtContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def NONTARG(self):
            return self.getToken(SentenceGrammarv2Parser.NONTARG, 0)
        def ent(self):
            return self.getTypedRuleContext(SentenceGrammarv2Parser.EntContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTargToNonTarg" ):
                return visitor.visitTargToNonTarg(self)
            else:
                return visitor.visitChildren(self)



    def et(self):

        localctx = SentenceGrammarv2Parser.EtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_et)
        try:
            self.state = 28
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [SentenceGrammarv2Parser.SEQ]:
                localctx = SentenceGrammarv2Parser.TargToSeqContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 22
                self.match(SentenceGrammarv2Parser.SEQ)
                self.state = 23
                self.esp()
                pass
            elif token in [SentenceGrammarv2Parser.GENE]:
                localctx = SentenceGrammarv2Parser.TargToGeneContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 24
                self.match(SentenceGrammarv2Parser.GENE)
                self.state = 25
                self.eg()
                pass
            elif token in [SentenceGrammarv2Parser.NONTARG]:
                localctx = SentenceGrammarv2Parser.TargToNonTargContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 26
                self.match(SentenceGrammarv2Parser.NONTARG)
                self.state = 27
                self.ent()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class EgContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return SentenceGrammarv2Parser.RULE_eg

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class GeneToSeqContext(EgContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SentenceGrammarv2Parser.EgContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def SEQ(self):
            return self.getToken(SentenceGrammarv2Parser.SEQ, 0)
        def esp(self):
            return self.getTypedRuleContext(SentenceGrammarv2Parser.EspContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGeneToSeq" ):
                return visitor.visitGeneToSeq(self)
            else:
                return visitor.visitChildren(self)


    class GeneToNonTargContext(EgContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SentenceGrammarv2Parser.EgContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def NONTARG(self):
            return self.getToken(SentenceGrammarv2Parser.NONTARG, 0)
        def ent(self):
            return self.getTypedRuleContext(SentenceGrammarv2Parser.EntContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGeneToNonTarg" ):
                return visitor.visitGeneToNonTarg(self)
            else:
                return visitor.visitChildren(self)


    class GeneFinishContext(EgContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SentenceGrammarv2Parser.EgContext
            super().__init__(parser)
            self.copyFrom(ctx)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGeneFinish" ):
                return visitor.visitGeneFinish(self)
            else:
                return visitor.visitChildren(self)


    class GeneToGeneContext(EgContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SentenceGrammarv2Parser.EgContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def GENE(self):
            return self.getToken(SentenceGrammarv2Parser.GENE, 0)
        def eg(self):
            return self.getTypedRuleContext(SentenceGrammarv2Parser.EgContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGeneToGene" ):
                return visitor.visitGeneToGene(self)
            else:
                return visitor.visitChildren(self)


    class GeneToTargContext(EgContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SentenceGrammarv2Parser.EgContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def TARG(self):
            return self.getToken(SentenceGrammarv2Parser.TARG, 0)
        def et(self):
            return self.getTypedRuleContext(SentenceGrammarv2Parser.EtContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGeneToTarg" ):
                return visitor.visitGeneToTarg(self)
            else:
                return visitor.visitChildren(self)



    def eg(self):

        localctx = SentenceGrammarv2Parser.EgContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_eg)
        try:
            self.state = 39
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [SentenceGrammarv2Parser.SEQ]:
                localctx = SentenceGrammarv2Parser.GeneToSeqContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 30
                self.match(SentenceGrammarv2Parser.SEQ)
                self.state = 31
                self.esp()
                pass
            elif token in [SentenceGrammarv2Parser.GENE]:
                localctx = SentenceGrammarv2Parser.GeneToGeneContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 32
                self.match(SentenceGrammarv2Parser.GENE)
                self.state = 33
                self.eg()
                pass
            elif token in [SentenceGrammarv2Parser.TARG]:
                localctx = SentenceGrammarv2Parser.GeneToTargContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 34
                self.match(SentenceGrammarv2Parser.TARG)
                self.state = 35
                self.et()
                pass
            elif token in [SentenceGrammarv2Parser.NONTARG]:
                localctx = SentenceGrammarv2Parser.GeneToNonTargContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 36
                self.match(SentenceGrammarv2Parser.NONTARG)
                self.state = 37
                self.ent()
                pass
            elif token in [SentenceGrammarv2Parser.EOF]:
                localctx = SentenceGrammarv2Parser.GeneFinishContext(self, localctx)
                self.enterOuterAlt(localctx, 5)

                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class EsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return SentenceGrammarv2Parser.RULE_es

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class SeqContext(EsContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SentenceGrammarv2Parser.EsContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def SEQ(self):
            return self.getToken(SentenceGrammarv2Parser.SEQ, 0)
        def esp(self):
            return self.getTypedRuleContext(SentenceGrammarv2Parser.EspContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSeq" ):
                return visitor.visitSeq(self)
            else:
                return visitor.visitChildren(self)



    def es(self):

        localctx = SentenceGrammarv2Parser.EsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_es)
        try:
            localctx = SentenceGrammarv2Parser.SeqContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 41
            self.match(SentenceGrammarv2Parser.SEQ)
            self.state = 42
            self.esp()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class EntContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return SentenceGrammarv2Parser.RULE_ent

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class NontargToSeqContext(EntContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SentenceGrammarv2Parser.EntContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def SEQ(self):
            return self.getToken(SentenceGrammarv2Parser.SEQ, 0)
        def esp(self):
            return self.getTypedRuleContext(SentenceGrammarv2Parser.EspContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNontargToSeq" ):
                return visitor.visitNontargToSeq(self)
            else:
                return visitor.visitChildren(self)



    def ent(self):

        localctx = SentenceGrammarv2Parser.EntContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_ent)
        try:
            localctx = SentenceGrammarv2Parser.NontargToSeqContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 44
            self.match(SentenceGrammarv2Parser.SEQ)
            self.state = 45
            self.esp()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class EspContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return SentenceGrammarv2Parser.RULE_esp

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class SeqFinishContext(EspContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SentenceGrammarv2Parser.EspContext
            super().__init__(parser)
            self.copyFrom(ctx)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSeqFinish" ):
                return visitor.visitSeqFinish(self)
            else:
                return visitor.visitChildren(self)


    class SeqToTargContext(EspContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SentenceGrammarv2Parser.EspContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def TARG(self):
            return self.getToken(SentenceGrammarv2Parser.TARG, 0)
        def et(self):
            return self.getTypedRuleContext(SentenceGrammarv2Parser.EtContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSeqToTarg" ):
                return visitor.visitSeqToTarg(self)
            else:
                return visitor.visitChildren(self)


    class SeqToGeneContext(EspContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SentenceGrammarv2Parser.EspContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def GENE(self):
            return self.getToken(SentenceGrammarv2Parser.GENE, 0)
        def eg(self):
            return self.getTypedRuleContext(SentenceGrammarv2Parser.EgContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSeqToGene" ):
                return visitor.visitSeqToGene(self)
            else:
                return visitor.visitChildren(self)


    class SeqToSeqContext(EspContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SentenceGrammarv2Parser.EspContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def SEQ(self):
            return self.getToken(SentenceGrammarv2Parser.SEQ, 0)
        def esp(self):
            return self.getTypedRuleContext(SentenceGrammarv2Parser.EspContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSeqToSeq" ):
                return visitor.visitSeqToSeq(self)
            else:
                return visitor.visitChildren(self)


    class SeqToNontargContext(EspContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SentenceGrammarv2Parser.EspContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def NONTARG(self):
            return self.getToken(SentenceGrammarv2Parser.NONTARG, 0)
        def es(self):
            return self.getTypedRuleContext(SentenceGrammarv2Parser.EsContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSeqToNontarg" ):
                return visitor.visitSeqToNontarg(self)
            else:
                return visitor.visitChildren(self)



    def esp(self):

        localctx = SentenceGrammarv2Parser.EspContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_esp)
        try:
            self.state = 56
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [SentenceGrammarv2Parser.TARG]:
                localctx = SentenceGrammarv2Parser.SeqToTargContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 47
                self.match(SentenceGrammarv2Parser.TARG)
                self.state = 48
                self.et()
                pass
            elif token in [SentenceGrammarv2Parser.NONTARG]:
                localctx = SentenceGrammarv2Parser.SeqToNontargContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 49
                self.match(SentenceGrammarv2Parser.NONTARG)
                self.state = 50
                self.es()
                pass
            elif token in [SentenceGrammarv2Parser.GENE]:
                localctx = SentenceGrammarv2Parser.SeqToGeneContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 51
                self.match(SentenceGrammarv2Parser.GENE)
                self.state = 52
                self.eg()
                pass
            elif token in [SentenceGrammarv2Parser.SEQ]:
                localctx = SentenceGrammarv2Parser.SeqToSeqContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 53
                self.match(SentenceGrammarv2Parser.SEQ)
                self.state = 54
                self.esp()
                pass
            elif token in [SentenceGrammarv2Parser.EOF]:
                localctx = SentenceGrammarv2Parser.SeqFinishContext(self, localctx)
                self.enterOuterAlt(localctx, 5)

                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





