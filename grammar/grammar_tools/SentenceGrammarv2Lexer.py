# Generated from SentenceGrammarv2.g4 by ANTLR 4.7.2
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\7")
        buf.write("A\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\3\2\3\2")
        buf.write("\6\2\20\n\2\r\2\16\2\21\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\3\3\3\6\3\"\n\3\r\3\16\3#\3\4\3\4")
        buf.write("\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\6\4\61\n\4\r\4\16")
        buf.write("\4\62\3\5\3\5\6\5\67\n\5\r\5\16\58\3\6\6\6<\n\6\r\6\16")
        buf.write("\6=\3\6\3\6\2\2\7\3\3\5\4\7\5\t\6\13\7\3\2\6\4\2UUuu\3")
        buf.write("\2\62;\4\2IIii\5\2\13\f\17\17\"\"\2E\2\3\3\2\2\2\2\5\3")
        buf.write("\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\3\r\3\2\2")
        buf.write("\2\5\23\3\2\2\2\7%\3\2\2\2\t\64\3\2\2\2\13;\3\2\2\2\r")
        buf.write("\17\t\2\2\2\16\20\t\3\2\2\17\16\3\2\2\2\20\21\3\2\2\2")
        buf.write("\21\17\3\2\2\2\21\22\3\2\2\2\22\4\3\2\2\2\23\24\7p\2\2")
        buf.write("\24\25\7q\2\2\25\26\7p\2\2\26\27\7v\2\2\27\30\7c\2\2\30")
        buf.write("\31\7t\2\2\31\32\7i\2\2\32\33\7g\2\2\33\34\7v\2\2\34\35")
        buf.write("\7k\2\2\35\36\7p\2\2\36\37\7i\2\2\37!\3\2\2\2 \"\t\3\2")
        buf.write("\2! \3\2\2\2\"#\3\2\2\2#!\3\2\2\2#$\3\2\2\2$\6\3\2\2\2")
        buf.write("%&\7v\2\2&\'\7c\2\2\'(\7t\2\2()\7i\2\2)*\7g\2\2*+\7v\2")
        buf.write("\2+,\7k\2\2,-\7p\2\2-.\7i\2\2.\60\3\2\2\2/\61\t\3\2\2")
        buf.write("\60/\3\2\2\2\61\62\3\2\2\2\62\60\3\2\2\2\62\63\3\2\2\2")
        buf.write("\63\b\3\2\2\2\64\66\t\4\2\2\65\67\t\3\2\2\66\65\3\2\2")
        buf.write("\2\678\3\2\2\28\66\3\2\2\289\3\2\2\29\n\3\2\2\2:<\t\5")
        buf.write("\2\2;:\3\2\2\2<=\3\2\2\2=;\3\2\2\2=>\3\2\2\2>?\3\2\2\2")
        buf.write("?@\b\6\2\2@\f\3\2\2\2\b\2\21#\628=\3\b\2\2")
        return buf.getvalue()


class SentenceGrammarv2Lexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    SEQ = 1
    NONTARG = 2
    TARG = 3
    GENE = 4
    WS = 5

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
 ]

    symbolicNames = [ "<INVALID>",
            "SEQ", "NONTARG", "TARG", "GENE", "WS" ]

    ruleNames = [ "SEQ", "NONTARG", "TARG", "GENE", "WS" ]

    grammarFileName = "SentenceGrammarv2.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


