# Generated from SentenceGrammarv2.g4 by ANTLR 4.7.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .SentenceGrammarv2Parser import SentenceGrammarv2Parser
else:
    from SentenceGrammarv2Parser import SentenceGrammarv2Parser

# This class defines a complete generic visitor for a parse tree produced by SentenceGrammarv2Parser.

class SentenceGrammarv2Visitor(ParseTreeVisitor):

    # Visit a parse tree produced by SentenceGrammarv2Parser#firstTarg.
    def visitFirstTarg(self, ctx:SentenceGrammarv2Parser.FirstTargContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SentenceGrammarv2Parser#firstNonTarg.
    def visitFirstNonTarg(self, ctx:SentenceGrammarv2Parser.FirstNonTargContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SentenceGrammarv2Parser#firstGene.
    def visitFirstGene(self, ctx:SentenceGrammarv2Parser.FirstGeneContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SentenceGrammarv2Parser#firstSeq.
    def visitFirstSeq(self, ctx:SentenceGrammarv2Parser.FirstSeqContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SentenceGrammarv2Parser#targToSeq.
    def visitTargToSeq(self, ctx:SentenceGrammarv2Parser.TargToSeqContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SentenceGrammarv2Parser#targToGene.
    def visitTargToGene(self, ctx:SentenceGrammarv2Parser.TargToGeneContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SentenceGrammarv2Parser#targToNonTarg.
    def visitTargToNonTarg(self, ctx:SentenceGrammarv2Parser.TargToNonTargContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SentenceGrammarv2Parser#geneToSeq.
    def visitGeneToSeq(self, ctx:SentenceGrammarv2Parser.GeneToSeqContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SentenceGrammarv2Parser#geneToGene.
    def visitGeneToGene(self, ctx:SentenceGrammarv2Parser.GeneToGeneContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SentenceGrammarv2Parser#geneToTarg.
    def visitGeneToTarg(self, ctx:SentenceGrammarv2Parser.GeneToTargContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SentenceGrammarv2Parser#geneToNonTarg.
    def visitGeneToNonTarg(self, ctx:SentenceGrammarv2Parser.GeneToNonTargContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SentenceGrammarv2Parser#geneFinish.
    def visitGeneFinish(self, ctx:SentenceGrammarv2Parser.GeneFinishContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SentenceGrammarv2Parser#seq.
    def visitSeq(self, ctx:SentenceGrammarv2Parser.SeqContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SentenceGrammarv2Parser#nontargToSeq.
    def visitNontargToSeq(self, ctx:SentenceGrammarv2Parser.NontargToSeqContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SentenceGrammarv2Parser#seqToTarg.
    def visitSeqToTarg(self, ctx:SentenceGrammarv2Parser.SeqToTargContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SentenceGrammarv2Parser#seqToNontarg.
    def visitSeqToNontarg(self, ctx:SentenceGrammarv2Parser.SeqToNontargContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SentenceGrammarv2Parser#seqToGene.
    def visitSeqToGene(self, ctx:SentenceGrammarv2Parser.SeqToGeneContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SentenceGrammarv2Parser#seqToSeq.
    def visitSeqToSeq(self, ctx:SentenceGrammarv2Parser.SeqToSeqContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SentenceGrammarv2Parser#seqFinish.
    def visitSeqFinish(self, ctx:SentenceGrammarv2Parser.SeqFinishContext):
        return self.visitChildren(ctx)



del SentenceGrammarv2Parser