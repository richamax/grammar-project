import re
import collections
from grammar_exec import *
from sentence_transformator import *
from bs4 import BeautifulSoup

not_end = ["Inc.", "Ltd.", "inc.", "ltd.", "Co.", "co.", "no."]

targeting_list = ["targeting", "primer", "silencing", "primers", "target", "specific", "positive-control", "positive control", "internal control", "endogenous control"]

nottargeting_list = ["nonsilencing", "non-silencing", "non-targeting", "random", "scrambled", "nontargeting", "shCtrl",
                      "scramble", "nonspecific", "scr", "negative-control", "non-specific", "Scrambled", "non silencing", "negative control", "non targeting", "non specific", "control"]


def is_special_gene(current_word, position, list_gene, list_words):
    """
    Function that given the current word and his position, determine if the word correspond to a gene that have been
    cut by the pre-processing algorithm
    :param current_word: word we are currently considering
    :param position: postion of the word in list_words
    :param list_gene: list containing the gene already seen
    :param list_words: list of the words of the sentence
    :return: the list of gene and a boolean, the boolean tells us if the list of gene has been modify or not
    """
    next = ""
    if current_word != list_words[-1]:
        next = list_words[position+1]

    if current_word.upper() == "MIR":
        if re.match('^[0-9]+$', next):
            list_gene.append(current_word + "-" + next)
        else:
            list_gene.append(current_word)

    elif current_word.upper() == "CYCLIN":
        list_gene.append(current_word + "-" + next)

    elif current_word.upper() == "MMP":
        if re.match('^[0-9]+$', next):
            list_gene.append(current_word + next)

    elif current_word.upper() == "AEG":
        if re.match('^[0-9]+$', next):
            list_gene.append(current_word + "-" + next)

    elif current_word.upper() == "COX":
        if re.match('^[0-9]+$', next):
            list_gene.append(current_word + "-" + next)

    elif current_word.upper() == "BCL":

        if re.match('^[0-9]+$', next):
            list_gene.append(current_word + "-" + next)

        elif next == "XL":
            list_gene.append(current_word + "-" + next)

    elif current_word.upper() == "C":
        if next.upper() == "MYC":
            list_gene.append(current_word + "-" + next)
        else:
            return list_gene, False

    elif current_word.upper() == "UTR":
        if next.upper() == "KRAS":
            list_gene.append(current_word + "-" + next)
        else:
            return list_gene, False

    else:
        return list_gene, False

    return list_gene, True


def get_text_bs(html):
    """
    Extract the text from a hmtl file
    :param html: html file from which we want to extract the text
    :return: the text of a html file
    """
    tree = BeautifulSoup(html, 'lxml')

    body = tree.body
    if body is None:
        return None

    for tag in body.select('script'):
        tag.decompose()
    for tag in body.select('style'):
        tag.decompose()

    text = body.get_text(separator='\n')
    return text


def create_phrases(text):
    """
    This function find the sentences present in a text
    :param text: text from which we wqnt to extract the sentences
    :return: list of sentences found in the text
    """
    phrases = []
    sentence = ""
    end_w = True
    for line in nonblank_lines(text):
        all_words = re.split(" |-|\t", line)
        for word in all_words:
            longeur = str(len(word))
            if len(word) > 1:
                selected = re.findall(r"[ATCGUatcgu]{" + longeur + ",}", word)
            else:
                selected = None
            # if the word is composed of only letter suc as ATCGUatcgu we continue
            if selected:
                # we retrive the postion of the word in the sentence
                position_word = all_words.index(word)
                extended_word = word
                # we iterate over the remaning words
                while position_word != len(all_words) - 1:
                    next_word = all_words[position_word + 1]
                    longeur_cutted = str(len(next_word))
                    cut = re.findall(r"[ATCGUatcgu]{" + longeur_cutted + ",}", next_word)
                    # we check if the next word is also composed of letter such as ATCGUatcgu
                    # If it is the case that mean the ARN sequence has been cutted, so we merge them
                    # and we delet the second part of the sequence
                    # if it is not the case we stop the loop
                    if cut:
                        del all_words[position_word + 1]
                        extended_word += cut[0]
                    else:
                        break
                # v2
                all_words[all_words.index(word)] = extended_word
                word = extended_word

            sentence += word + " "

        all_words = re.split(" |\n", sentence)

        sentence = ""

        for word in all_words:
            if "." in word:
                for spe_word in not_end:
                    if spe_word in word and end_w:
                        sentence += word + " "
                        end_w = False
                if word[-1] != "." and end_w:
                    sentence += word + " "
                    end_w = False
                if end_w:
                    sentence += word
                    phrases.append(sentence)
                    sentence = ""
                end_w = True
            elif word != "":
                sentence += word + " "

    return phrases


def nonblank_lines(f):
    """
    yield each line of a file that is not a blank line
    :param f: file from which we want to remove the blank lines
    :return:
    """
    for l in f:
        line = l.rstrip()
        if line:
            yield line


def key_to_string(key_word):
    """
    Take as input an ordered dictionary and creat a string containing all of the keys of the dictionary
    :param key_word: ordered dictionary containing key_words of the sentence
    :return:
    """
    string = ""
    for elem in key_word.keys():
        string += elem + " "
    return string


def replace_in_list(dico, lists):
    """
    Replace in lists the key words by their real meaning in the sentence
    :param dico: dictionary containing the real value in the sentence of the keyword
    :param lists: list containing all of the information for each sequences found in the sentence
    :return: the same list but with the key word replaced
    """
    final_list = []
    for l in lists:
        current_list = []
        for i in range(3):
            if i == 0 or i == 1:
                elem = l[i]
                if l[i] in dico:
                    current_list.append(dico[elem])
                else:
                    current_list.append(elem)
            else:
                sublist = []
                if l[2]:
                    for elem in l[2]:
                        if elem in dico.keys():
                            sublist.append(dico[elem])
                        else:
                            sublist.append(None)
                    current_list.append(sublist)
                else:
                    current_list.append(None)
        final_list.append(current_list)
    return final_list


def seq_in_final_list(elem, list):
    """
    Go through all of the sequence found in the sentence, and return the sub_list containing the information we extracted
    for the text if we have found it
    :param elem: sequence that we want to find in list
    :param list: list containing the information found for each sequence found in the sentence
    :return: either None if the sequence is not present in list either the information we extracted about this sequence
    """
    for sub_l in list:
        if sub_l[2]:
            if elem in sub_l[2]:
                return sub_l
    return None


def split_sub_list(main_list):
    """
    Go through all of the sublist present in min_list, if one of the sublist have multiple sequences we split it in
    multiple sublist
    :param main_list: containing all of the information extracted from the sentence
    :return: the main list modify such that each sub list only contains one sequence
    """
    modified_list = []
    for sub in main_list:
        if sub[2]:
            for elem in sub[2]:
                modified_list.append([sub[0], sub[1], [elem]])
        else:
            modified_list.append([sub[0], sub[1], None])

    return modified_list


def file_in_gold(gold):
    """
    Go through the gold file and extract the name of all of the files for which the information have been extracted
    from the text
    :param gold: reference file containing the information extracted from each text by human
    :return: A list containing the name of each file that have been treated in the Gold file
    """
    files_gold = []
    with open(gold, 'r') as g:
        for line in nonblank_lines(g):
            if line[0] != "#":
                id = line.split("\t")[0]
                if id not in files_gold:
                    files_gold.append(id)
    return files_gold


def main():
    """
    Fonction qui prend en arguments le nom du corpus et le dossier contenant les differents fichiers
    html à traiter
    :return: Ecrit deux fichier, l'un contant tout les sequences extraites des differents articles:
    -> extracted_sentences_html.txt
    et un autre contant les informations (seq status gene) contenu dans les differentes phrases:
    -> extracted_information_html.txt
    """
    if len(sys.argv) < 4:
        print("Usage : python3 pipeline.py name_of_corpus path_to_gold_file folder_containing_html_to_process/*")
        return

    # define the name of the output files
    information = "pipeline_output/extracted_information_html.txt"
    exact_sentences = "pipeline_output/exacte_sentences_html.txt"
    sentences_file = "pipeline_output/extracted_sentences_html.txt"

    corp_name = sys.argv[1]

    gold = sys.argv[2]

    # Retrieve all of the files analysed in Gold.txt
    gold_files = file_in_gold(gold)
    fill_gene_dic("grammar_tools/Homo_sapiens.gene_info.txt")

    # formatage des fichiers
    with open(information, "a") as f:
        f.write("###########\n## " + corp_name + "\n###########\n\n")
        f.write("#PMID\tSID\tSEQ\tSTATUS\tGENE\n\n")
    with open(sentences_file, "a") as f:
        f.write("###########\n## " + corp_name + "\n###########\n\n")

        # we go through every file of the folder
        for fichier in sys.argv[3:]:
            if ".html" in fichier:
                # retreive the ID of the PDF file
                folder = fichier.split("/")[0]
                number = fichier.split("/")[1].split(".")[0]
                file_name = number + ".pdf"
                # if the file has been analysed in gold we continue
                if number in gold_files:
                    print("Extracting DNA and RNA sequences form " + file_name + ", in " + folder + "\n")
                    sys.stderr.write("Extracting DNA and RNA sequences form " + file_name + ", in " + folder + "\n")
                    # id of the current sentence
                    sid = 1

                    first_opening = True
                    current_file = open(fichier, "rb")
                    text = get_text_bs(current_file)

                    # break into lines and remove leading and trailing space on each
                    lines = (line.strip() for line in text.splitlines())
                    # break multi-headlines into a line each
                    chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
                    # drop blank lines
                    text = '\n'.join(chunk for chunk in chunks if chunk)

                    text = text.split("\t")

                    # replacing the "\n" with " " same for "" create by the previous splittings
                    for elem in text:
                        if elem == "\n":
                            elem.replace("\n", " ")
                        elif elem == "":
                            elem.replace("", " ")

                    # extracting the sentences from the text
                    phrases = create_phrases(text)

                    # Iterating over all of the sentences of the text
                    for phrase in phrases:
                        seen_seq = {}

                        # retrieving all of the word of the sentence
                        words = re.split(' |\(|\)|-|,|!|\?|;|:|\.|‐|"', phrase)

                        # This list will contain all nucleotide sequences from a sentence if any
                        list_word = []

                        # we iterate over all the word of the sentence
                        for word in words:
                            if re.match(r"[ATCGUatcgu]{8,}", word):
                                list_word.append(word)
                        # If the list of word is not empty that means the sentence contains at least one nucleotide
                        # sequence
                        if list_word:
                            # We recreate the sentence without the special symbols such as "-" or ","...
                            reformed_sentence = ""
                            for elem in words:
                                reformed_sentence += elem + " "

                            # We store this sentence in the final output file
                            with open(sentences_file, "a") as f_sen:
                                if first_opening:
                                    f_sen.write("####################\n")
                                    f_sen.write("#" + file_name + "\n")
                                    f_sen.write("####################\n\n")
                                # v1
                                # f_sen.write(number+"_S"+str(sid)+"\t"+phrase+"\n\n")
                                f_sen.write(number + "_S" + str(sid) + "\t")
                                f_sen.write(reformed_sentence)
                                f_sen.write(".\n\n")

                            # We store the exact sentence to be able to double check the result of the algorithm
                            with open(exact_sentences, "a") as exact:
                                exact.write(phrase + "\n")

                            # Then we are going to process th information of the sentence and store them in the
                            # information file
                            with open(information, "a") as result:
                                if first_opening:
                                    result.write("#" + file_name + "\n\n")
                                    first_opening = False

                                line_no_dot = reformed_sentence.split('.')[0]

                                s_words = line_no_dot.split(" ")

                                for elem in s_words:
                                    if elem == "":
                                        s_words.remove(elem)

                                # here we modify the sentence in case it contains special gene such as "miR-127" which
                                # have lost the "-" du to the pre processing of the sentences
                                updated_gene = []

                                new_line = ""

                                for pos, elem in enumerate(s_words):
                                    updated_gene, found = is_special_gene(elem, pos, updated_gene, s_words)
                                    if found:
                                        s_words[pos] = updated_gene[0]
                                        del s_words[pos + 1]
                                    updated_gene = []

                                for elem in s_words:
                                    new_line += elem + " "

                                # After that we use the word tagging algorithm to extract the word of interest from
                                # the sentence
                                key_word = sentence_transform(new_line, genes)

                                # we convert the list of keyword into a string
                                string = key_to_string(key_word)

                                # the string is passed down to the grammar which will try to extract the information
                                # from the sentence
                                list_of_dependence = grammaire_string(string)

                                # The keywords contained in the output list produced by the grammar are replaced
                                # by their real value in the text
                                final_list = replace_in_list(key_word, list_of_dependence)

                                # The sublist contained in the final list are splitted if they contain more than one
                                #  nucleotide sequence
                                final_list = split_sub_list(final_list)

                                # For each sequence contain in the sentence, we create a new line in the final output file
                                # containing the information extracted from the sentence by the grammar
                                for seq in list_word:
                                    sub_list = seq_in_final_list(seq, final_list)

                                    # if we were able to extract information for this sentence we write it
                                    # either we say that the status and gene are unknown from us
                                    if sub_list:
                                        # if number not in seen_seq.keys():
                                        #     seen_seq[number] = []
                                        # if seq not in seen_seq[number]:
                                        #     seen_seq[number].append(seq)
                                        #     occ = 1
                                        # else:
                                        #     occ += 1
                                        #     seq += " (" + str(occ) + ")"
                                        if sub_list[1] is None:
                                            my_gene = "none"
                                        elif sub_list[1] == "actin" or sub_list[1] == "Actin" or sub_list[1] == "ACTIN":
                                            my_gene = "ACTB"
                                        elif sub_list[1] == "CyclinD1":
                                            my_gene = "CCND1"
                                        elif sub_list[1] == "CyclinA":
                                            my_gene = "CCNA2"
                                        else:
                                            my_gene = sub_list[1]

                                        if sub_list[0] in nottargeting_list:
                                            status = "non-targeting"
                                        else:
                                            status = "targeting"
                                        my_gene = my_gene.upper() if my_gene != "none" else my_gene
                                        result.write(number + "\t" + number + "_S" + str(sid) + "\t" + seq + "\t" + status + "\t" + my_gene + "\n")
                                        final_list.remove(sub_list)
                                    else:
                                        result.write(number + "\t" + number + "_S" + str(sid) + "\t" + seq + "\tunknown\tunknown\n")
                                result.write("\n")
                            sid += 1


if __name__ == "__main__":
    # execute only if run as a script
    main()