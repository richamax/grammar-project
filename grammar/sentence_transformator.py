import sys
import re
import collections

targeting_list = ["targeting", "primer", "silencing", "primers", "target", "specific", "positive-control"]
targeting_list_double = ["positive control", "internal control", "endogenous control"]

nottargeting_list = ["nonsilencing", "non-silencing", "non-targeting", "random", "scrambled", "nontargeting", "shCtrl",
                      "scramble", "nonspecific", "scr", "negative-control", "non-specific", "Scrambled", "Nontargeting",
                     "Nonsilencing"]

nottargeting_list_double = ["non silencing", "negative control", "non targeting", "non specific"]

# Dictionnary of all possible gene, initialy empty

genes = {}


def fill_gene_dic(file_path):
    with open(file_path, "r") as file:
        file.readline()
        for line in file:
            line = line[:-1]
            splitted_line = line.split("\t")
            main = splitted_line[2]
            list_synonyms = splitted_line[4].split("|")
            description = splitted_line[8]
            symb_nomenclature = splitted_line[10]
            genes[main] = (main,) + tuple(list_synonyms) + (description,) + (symb_nomenclature,)
            for elem in list_synonyms:
                genes[elem] = (main,) + tuple(list_synonyms) + (description,) + (symb_nomenclature,)
            genes[description] = (main,) + tuple(list_synonyms) + (description,) + (symb_nomenclature,)
            genes[symb_nomenclature] = (main,) + tuple(list_synonyms) + (description,) + (symb_nomenclature,)

        # ajout list initiale (Co, Get, Mice, at, base, si, s3, fl, pr, utr, ct, mix, cel, D1, L19)
        not_gene = ["AS", "OF", "TS", "TES", "ON", "NM", "GMA", "CELL", "WAS", "MUT", "BP", "NC", "TYPE", "SYMBOLE",
                    "AN",
                    "IN", "RNA", "P", "MB", "OUT", "REF", "LI", "ALL", "FOR", "HAS", "NA", "FAST", "MICE", "2", "H",
                    "S",
                    "ME", "RT", "MAX", "ID", "TO", "B1", "S1", "S2", "NOT", "GET", "CO", "b", "B", "BLAST", "AT",
                    "BASE",
                    "SI", "S3", "FL", "PR", "UTR", "CT", "MIX", "CEL", "D1", "L19"]

    for elem in not_gene:
        if elem in genes.keys():
            del genes[elem]
    genes["CYCLINA"] = "CYCLINA"
    genes["CYCLIND1"] = "CYCLIND1"


def is_not_targeting(previous_word, current_word):
    seq = previous_word + " " + current_word
    if current_word in nottargeting_list:
        return True, 1
    elif seq in nottargeting_list_double:
        return True, 2
    return False, 0


def is_targeting(previous_word, current_word):
    seq = previous_word + " " + current_word
    if seq not in nottargeting_list_double and current_word in targeting_list:
        return True, 1
    elif seq in targeting_list_double:
        return True, 2
    return False, 0


def is_gene(current_word):
    global genes
    particular_gene = ["MIR", "CYCLIN", "AEG", "COX", "BCL", "C", "UTR"]
    if "-" in current_word:
        un,deux = current_word.split("-")
        if un.upper() in particular_gene:
            return True
    elif current_word.upper() == "ACTIN":
        return True
    return current_word.upper() in genes.keys()


def is_seq(current_word):
    if re.search("[AUCGTaucgt]{8,}", current_word):
        return True
    return False


def sentence_transform_from_terminal():
    sentence = sys.argv[1]
    fill_gene_dic("grammar_tools/Homo_sapiens.gene_info.txt")
    key_words = collections.OrderedDict()
    words = sentence.split(" ")
    previous_word = ""
    nb_gene = 0
    nb_seq = 0
    nb_nottarg = 0
    nb_targ = 0
    counter = 0
    for current_word in words:
        value_nt, nb_nt = is_not_targeting(previous_word, current_word)
        value_t, nb_t = is_targeting(previous_word, current_word)

        if current_word == "control":
            position = counter
            if words[position-1]:
                previous_word = words[position-1]
            else:
                previous_word = ""

            if words[position+1]:
                next_word = words[position+1]
            else:
                next_word = ""
            seq = previous_word + " " + current_word
            if seq not in nottargeting_list_double and seq not in targeting_list_double and next_word != "non" and next_word != "negative" and next_word != "." and previous_word not in nottargeting_list and next_word not in nottargeting_list:
                nb_nottarg += 1
                key_words["nontargeting" + str(nb_nottarg)] = current_word

        if is_gene(current_word):
            nb_gene += 1
            key_words["G" + str(nb_gene)] = current_word

        elif is_seq(current_word):
            nb_seq += 1
            key_words["S" + str(nb_seq)] = current_word

        elif value_nt:
            if nb_nt == 1:
                nb_nottarg += 1
                key_words["nontargeting" + str(nb_nottarg)] = current_word
            else:
                comb = previous_word + " " + current_word
                nb_nottarg += 1
                key_words["nontargeting" + str(nb_nottarg)] = comb

        elif value_t:
            if nb_t == 1:
                nb_targ += 1
                key_words["targeting" + str(nb_targ)] = current_word
            else:
                comb = previous_word + " " + current_word
                nb_targ += 1
                key_words["targeting" + str(nb_targ)] = comb

        previous_word = current_word
        counter += 1
    print(key_words)

def sentence_transform(sentence, gene_dico):
    """

    :param sentence: sentence we want to transform in a sequence of targeting G1 S1 ...s
    :return: dictionary containing the label of the key words of the sentence
    """
    global genes
    genes = gene_dico
    key_words = collections.OrderedDict()
    words = sentence.split(" ")
    previous_word = ""
    nb_gene = 0
    nb_seq = 0
    nb_nottarg = 0
    nb_targ = 0
    counter = 0
    for current_word in words:
        value_nt, nb_nt = is_not_targeting(previous_word, current_word)
        value_t, nb_t = is_targeting(previous_word, current_word)

        if current_word == "control":
            position = counter
            if position < len(words)-1:
                if words[position - 1]:
                    previous_word = words[position - 1]
                else:
                    previous_word = ""

                if words[position + 1]:
                    next_word = words[position + 1]
                else:
                    next_word = ""
                seq = previous_word + " " + current_word
                if seq not in nottargeting_list_double and seq not in targeting_list_double and next_word != "non" and next_word != "negative" and next_word != "." and previous_word not in nottargeting_list and next_word not in nottargeting_list:
                    nb_nottarg += 1
                    key_words["nontargeting" + str(nb_nottarg)] = current_word

        if is_gene(current_word):
            nb_gene += 1
            key_words["G" + str(nb_gene)] = current_word

        elif is_seq(current_word):
            nb_seq += 1
            key_words["S" + str(nb_seq)] = current_word

        elif value_nt:
            if nb_nt == 1:
                nb_nottarg += 1
                key_words["nontargeting" + str(nb_nottarg)] = current_word
            else:
                comb = previous_word + " " + current_word
                nb_nottarg += 1
                key_words["nontargeting" + str(nb_nottarg)] = comb

        elif value_t:
            if nb_t == 1:
                nb_targ += 1
                key_words["targeting" + str(nb_targ)] = current_word
            else:
                comb = previous_word + " " + current_word
                nb_targ += 1
                key_words["targeting" + str(nb_targ)] = comb

        previous_word = current_word
        counter += 1
    return key_words



if __name__ == "__main__":
    # execute only if run as a script
    sentence_transform_from_terminal()
