#! /bin/sh

echo "Removing existing files :\n>pipeline_output/exacte_sentences_html.txt\n>pipeline_output/extracted_information_html.txt\n>pipeline_output/extracted_sentences_html.txt\n>error_output/pipeline_error_msg.txt\n>error_output/perf_output.txt\n"

rm pipeline_output/exacte_sentences_html.txt pipeline_output/extracted_information_html.txt pipeline_output/extracted_sentences_html.txt error_output/pipeline_error_msg.txt error_output/perf_output.txt

echo "Specify path to Gold.txt file (i.e. : ../SeekAndBlastn/Listes/Gold.txt):"

read goldpath

echo "##################################################"
echo "Pipeline is running"
echo "##################################################"

start=`date +%s`
python3 pipeline.py corpus_m $goldpath html_files/* 2> error_output/pipeline_error_msg.txt
end=`date +%s`
res_pipe=`expr $end - $start`
#echo Execution time of the pipeline was `expr $end - $start` seconds.

echo "##################################################"
echo "Evalutation of the results"
echo "##################################################"

start=`date +%s`
python3 performance_testv2.py pipeline_output/extracted_information_html.txt pipeline_output/extracted_sentences_html.txt $goldpath html_files/* 2> error_output/perf_output.txt
end=`date +%s`
res_test=`expr $end - $start`
echo "#######################################################"
echo "### Execution time of the pipeline was $res_pipe seconds.  ###"
echo "### ----------------------------------------------- ###"
echo "### The evaluation of the results took $res_test seconds.   ###"
echo "#######################################################"
