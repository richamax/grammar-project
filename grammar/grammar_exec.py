import sys
from antlr4 import *
# from SentenceGrammarLexer import SentenceGrammarLexer
# from SentenceGrammarParser import SentenceGrammarParser
from grammar_tools.SentenceGrammarv2Lexer import SentenceGrammarv2Lexer
from grammar_tools.SentenceGrammarv2Parser import SentenceGrammarv2Parser
from MyVisitorv2 import MyVisitorv2


def grammaire_exec():

    input_stream = FileStream(sys.argv[1])
    lexer = SentenceGrammarv2Lexer(input_stream)
    stream = CommonTokenStream(lexer)
    parser = SentenceGrammarv2Parser(stream)
    tree = parser.e()
    visitor = MyVisitorv2()
    res = visitor.visit(tree)
    print(res)


def grammaire_string(string):

    input_stream = InputStream(string)
    lexer = SentenceGrammarv2Lexer(input_stream)
    stream = CommonTokenStream(lexer)
    parser = SentenceGrammarv2Parser(stream)
    tree = parser.e()
    visitor = MyVisitorv2()
    res = visitor.visit(tree)
    return res


if __name__ == "__main__":
    # execute only if run as a script
    grammaire_exec()