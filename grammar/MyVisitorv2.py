from grammar_tools.SentenceGrammarv2Visitor import SentenceGrammarv2Visitor
from grammar_tools.SentenceGrammarv2Parser import SentenceGrammarv2Parser
import re
import sys


class MyVisitorv2(SentenceGrammarv2Visitor):
    def __init__(self):
        self.lists = [[None, None, None]]

    # Visit a parse tree produced by SentenceGrammarv2Parser#firstTarg.
    def visitFirstTarg(self, ctx):
        """
        visitor function for rule first targ
        :param ctx:
        :return: the list containing the extracted information from the sentence
        """
        value = self.visit(ctx.et())
        targ = ctx.TARG().getText()
        sys.stderr.write(str(value) + "\n")
        if value is None:
            pass
            # sys.stderr.write("cas non traité in first targ ", value)

        # elif value[0] == "gene_seq":
        #     position = value[1]
        #     self.lists[position][0] = targ
        #
        # elif value[0] == "seq_gene":
        #     position = value[1]
        #     self.lists[position][0] = targ
        #
        # elif value[0] == "gene_seq2":
        #     position = value[1]
        #     self.lists[position][0] = targ

        elif value[0] == "seq_seq":
            position = value[1]
            self.lists[position][0] = targ

        elif value[0] == "gene_seq_list":
            seq_list = value[2]
            gene_list = value[1]

            seq_p_gene = int(len(seq_list) / len(gene_list))
            count = 0
            for gene in gene_list:
                if self.lists[-1][1] is None and self.lists[-1][0] != "nontargeting":
                    self.lists[-1][0] = targ
                    self.lists[-1][1] = gene
                    self.lists[-1][2] = seq_list[count:count + seq_p_gene]
                    count += seq_p_gene
                else:
                    self.lists.append([targ, gene, seq_list[count:count + seq_p_gene]])
                    count += seq_p_gene

        elif value[0] == "tar_gene_seq":
            self.lists[value[1]][0] = targ

        elif value[0] == "nt_seq_list":
            seq_list = value[1]
            nt = value[2]
            # if self.lists[-1][2] is None and self.lists[-1][1] is None:
            #     self.lists[-1][0] = nt
            #     self.lists[-1][2] = [seq_list[-1]]
            # else:
            #     self.lists.append([nt, None, [seq_list[-1]]])
            #
            # for elem in seq_list[:-1]:
            #     self.lists.append([targ, None, [elem]])
            if self.lists[-1][2] is None and self.lists[-1][1] is None:
                self.lists[-1][0] = nt
                self.lists[-1][2] = seq_list
            else:
                self.lists.append([nt, None, seq_list])

        elif value[0] == "seq_list":
            seq_list = self.lists[value[1]][2]
            self.lists[value[1]][2] = None
            for l in seq_list:
                if self.lists[-1][2] is None:
                    self.lists[-1][2] = [l]
                    self.lists[-1][0] = targ
                else:
                    self.lists.append([targ, None, [l]])
        # self.lists[-1][0] = targ

        for elem in self.lists:
            if elem[1] and elem[0] is None:
                elem[0] = targ
            if elem[1] is None and elem[0] is None:
                elem[0] = "nontargeting"

        # sys.stderr.write(self.lists)
        return self.lists

    # Visit a parse tree produced by SentenceGrammarv2Parser#firstNonTarg.
    def visitFirstNonTarg(self, ctx):
        """
        visitor function for rule first non targeting
        :param ctx:
        :return: the list containing the extracted information from the sentence
        """
        value = self.visit(ctx.es())
        ntarg = ctx.NONTARG().getText()

        if value is None:
            pass

        elif value[0] == "seq":
            if self.lists[-1][2] is None:
                self.lists[-1][0] = ntarg
                self.lists[-1][2] = [value[1]]
            else:
                self.lists.append([ntarg, None, [value[1]]])

        elif value[0] == "seq_list":
            seq_lists = self.lists[value[1]][2]
            self.lists[value[1]][2] = None
            for seq in seq_lists:
                if self.lists[-1][2] is None:
                    self.lists[-1][0] = ntarg
                    self.lists[-1][2] = [seq]
                else:
                    self.lists.append([ntarg, None, [seq]])

        for elem in self.lists:
            if elem[1] and elem[0] is None:
                elem[0] = "targeting"
            if elem[1] is None and elem[0] is None:
                elem[0] = ntarg

        # sys.stderr.write(self.lists)
        return self.lists

    # Visit a parse tree produced by SentenceGrammarv2Parser#firstGene.
    def visitFirstGene(self, ctx):
        """
        visitor function for rule first gene
        :param ctx:
        :return: the list containing the extracted information from the sentence
        """
        value = self.visit(ctx.eg())
        gene = ctx.GENE().getText()

        if value is None:
            if self.lists[-1][1] is None and self.lists[-1][2] is None:
                self.lists[-1][1] = gene
            else:
                self.lists.append([None, gene, None])

        elif value[0] == "seq" or value[0] == "seq_seq":
            if value[1]:
                position = value[2]
                self.lists[position][1] = gene
            else:
                if value[0] == "seq_seq":
                    seqs = value[2]
                else:
                    seqs = [value[2]]
                self.lists.append(["targeting", gene, seqs])

        elif value[0] == "gene_seq":
            pass

        elif value[0] == "targ_seq":
            position = value[1]
            self.lists[position][1] = gene
            for subl in self.lists:
                stat = subl[0]
                if stat:
                    if re.match("[targeting]+[0-9]+$", stat) and subl[2]:
                        subl[1] = gene

        elif value[0] == "seq_list":
            position = value[1]
            self.lists[position][1] = gene

        elif value[0] == "seq_list_gene":
            seq_pos = value[2]
            nb_seq = len(self.lists[seq_pos][2])
            gene_list = [gene] + value[1]
            nb_gene = len(gene_list)
            seq_p_gene = int(nb_seq/nb_gene)
            seq_list = self.lists[seq_pos][2]
            self.lists[seq_pos][2] = None
            count = 0
            for gene in gene_list:
                if self.lists[-1][1] is None and self.lists[-1][0] != "nontargeting":
                    self.lists[-1][1] = gene
                    self.lists[-1][2] = seq_list[count:count+seq_p_gene]
                    count += seq_p_gene
                else:
                    self.lists.append([None, gene, seq_list[count:count+seq_p_gene]])
                    count += seq_p_gene

        elif value[0] == "targ_seq_list":
            seq_list = value[1]
            targ = value[2]
            if self.lists[-1][1] is None and self.lists[-1][2] is None:
                self.lists[-1][0] = targ
                self.lists[-1][1] = gene
                self.lists[-1][2] = seq_list
            else:
                self.lists.append([targ, gene, seq_list])

        for elem in self.lists:
            if elem[1] and elem[0] is None:
                elem[0] = "targeting"
            if elem[1] is None and elem[0] is None:
                elem[0] = "nontargeting"
        # sys.stderr.write(self.lists)
        return self.lists

    # Visit a parse tree produced by SentenceGrammarv2Parser#firstSeq.
    def visitFirstSeq(self, ctx):
        """
        visitor function for rule first gene
        :param ctx:
        :return: the list containing the extracted information from the sentence
        """
        value = self.visit(ctx.esp())
        seq = ctx.SEQ().getText()

        if value is None:
            if self.lists[-1][2] is None:
                self.lists[-1][2] = [seq]
            else:
                self.lists.append([None, None, [seq]])

        elif value[0] == "seq":
            if not value[1]:
                self.lists.append([None, None, [value[2]]])
            self.lists.append([None, None, [seq]])

        elif value[0] == "gene":
            position = value[2]
            self.lists[position][2] = [seq]

        elif value[0] == "gene_list":
            if len(value[1])>1:
                gene = value[1][0]
            else:
                gene = value[1]
            if self.lists[-1][2] is None and self.lists[-1][0] is None and self.lists[-1][1] is None:
                self.lists[-1][1] = gene
                self.lists[-1][2] = [seq]

        elif value[0] == "seq_gene":
            position = value[1]
            self.lists[position][2].append(seq)

        elif value[0] == "targ_gene":
            position = value[1]
            self.lists[position][2] = [seq]

        elif value[0] == "targ_gene_list":
            if self.lists[-1][2] is None and self.lists[-1][0] is None and self.lists[-1][1] is None:
                self.lists[-1][0] = value[2]
                self.lists[-1][1] = value[1][0]
                self.lists[-1][2] = [seq]
            else:
                self.lists.append([value[2], value[1][0], [seq]])

        elif value[0] == "seq_list":
            seq_list = [seq] + self.lists[value[1]][2]
            self.lists[value[1]][2] = None

            for seq in seq_list:
                if self.lists[-1][2] is None:
                    self.lists[-1][2] = [seq]
                else:
                    self.lists.append(["targeting", None, [seq]])

        elif value[0] == "seq_gene_list":
            gene_list = value[2]
            seq_list = [seq] + value[1]
            seq_p_gene = int(len(seq_list)/len(gene_list))
            count = 0
            for gene in gene_list:
                if self.lists[-1][1] is None and self.lists[-1][0] != "nontargeting":
                    self.lists[-1][1] = gene
                    self.lists[-1][2] = seq_list[count:count + seq_p_gene]
                    count += seq_p_gene
                else:
                    self.lists.append([None, gene, seq_list[count:count + seq_p_gene]])
                    count += seq_p_gene

        elif value[0] == "seq_list_targ_gene":
            position = value[1]
            self.lists[position][2].append(seq)

        elif value[0] == "seq_targ_gene":
            position = value[1]
            self.lists[position][2].append(seq)

        else:
            sys.stderr.write("cas non traité first seq " + str(value) + "\n")
        for elem in self.lists:
            if elem[1] and elem[0] is None:
                elem[0] = "targeting"
            if elem[1] is None and elem[0] is None:
                elem[0] = "nontargeting"

        # sys.stderr.write(self.lists)
        return self.lists

    # Visit a parse tree produced by SentenceGrammarv2Parser#targToSeq.
    def visitTargToSeq(self, ctx):
        """
        visitor function for rule Targeting to sequence
        :param ctx:
        :return: send information to the father according to the value send by his children and his context
        """
        value = self.visit(ctx.esp())
        seq = ctx.SEQ().getText()
        if value is None:
            if self.lists[-1][1] is None and self.lists[-1][2] is None:
                position = -1
                self.lists[position][2] = [seq]
                return "seq", position
            else:
                return "seq", False, seq

        elif value[0] == "seq":
            if value[1]:
                position = value[2]
                self.lists[position][2].insert(0, seq)
                return "seq_seq", position
            else:
                sys.stderr.write("cas non traité T2S" + str(value) + "\n")

        elif value[0] == "gene":
            if value[1]:
                position = value[2]
                self.lists[position][2] = [seq]
                return "seq_gene", position
            else:
                pass

        elif value[0] == "seq_gene":
            position = value[1]
            self.lists[position][2].append(seq)
            return None

        elif value[0] == "seq_gene_list":
            seq_list = [seq] + value[1]
            return "gene_seq_list", value[2], seq_list

        elif value[0] == "seq_list":
            position = value[1]
            self.lists[position][2].append(seq)
            return "seq_list", position

        else:
            sys.stderr.write("case non géré dans visitTargToSeq" + str(value) + "\n")

    # Visit a parse tree produced by SentenceGrammarv2Parser#targToGene.
    def visitTargToGene(self, ctx):
        """
        visitor function for rule Targeting to gene
        :param ctx:
        :return: send information to the father according to the value send by his children and his context
        """
        value = self.visit(ctx.eg())
        gene = ctx.GENE().getText()

        if value is None:
            if self.lists[-1][1] is None and self.lists[-1][2] is None:
                position = -1
                self.lists[position][1] = gene
                return "gene", position
            else:
                position = -1
                self.lists.append([None, gene, None])
                return "gene", position

        elif value[0] == "seq":
            if value[1]:
                position = value[2]
                self.lists[position][1] = gene
            else:
                seq = value[2]
                if self.lists[-1][1] is None and self.lists[-1][2] is None:
                    self.lists[-1][1] = gene
                    self.lists[-1][2] = [seq]
                else:
                    self.lists.append([None, gene, [seq]])
                position = -1
            return "gene_seq", position

        elif value[0] == "seq_seq":
            if value[1]:
                position = value[2]
                self.lists[position][1] = gene
            else:
                seqs = value[2]
                position = -1
                self.lists.append([None, gene, seqs])
            return "gene_seq2", position

        elif value[0] == 'seq_list':
            self.lists[value[1]][1] = gene
            return "tar_gene_seq", value[1]

        elif value[0] == "gene":
            gene_list = value[1]
            gene_list.insert(0, gene)
            return "gene_list", gene_list

        elif value[0] == "seq_list_gene":
            seq_pos = value[2]
            seq_list = self.lists[seq_pos][2]
            self.lists[seq_pos][2] = None
            gene_list = [gene] + value[1]
            return "gene_seq_list", gene_list, seq_list

        elif value[0] == "targ_seq":
            position = value[1]
            self.lists[position][1] = gene
            return None

        else:
            sys.stderr.write("case non géré TargToGene" + str(value) + "\n")

    # Visit a parse tree produced by SentenceGrammarv2Parser#targToNonTarg.
    def visitTargToNonTarg(self, ctx):
        """
        visitor function for rule Targeting to non targeting
        :param ctx:
        :return: send information to the father according to the value send by his children and his context
        """
        value = self.visit(ctx.ent())
        ntarg = ctx.NONTARG().getText()

        if value is None:
            sys.stderr.write("cas non traité T2NT"+ str(value) + "\n")

        elif value[0] == "seq_list":
            position = value[1]
            seq_list = self.lists[position][2]
            self.lists[position][2] = None
            return "nt_seq_list", seq_list, ntarg

        elif value[0] == "seq":
            if value[1]:
                sys.stderr.write("cas non traité T2NT" + str(value) + "\n")
            else:
                seq = value[2]
                if self.lists[-1][0] is None and self.list[-1][2] is None:
                    self.lists[-1][0] = ntarg
                    self.lists[-1][2] = [seq]

                else:
                    self.lists.append([ntarg, None, [seq]])
                return None

        else:
            sys.stderr.write("cas non traité T2NT" + str(value) + "\n")

    # Visit a parse tree produced by SentenceGrammarv2Parser#geneToSeq.
    def visitGeneToSeq(self, ctx):
        """
        visitor function for rule Gene to sequence
        :param ctx:
        :return: send information to the father according to the value send by his children and his context
        """
        value = self.visit(ctx.esp())
        seq = ctx.SEQ().getText()
        # If the sequence we are dealing with is the last one
        # Then we put it in the only existing list at this time, that is the first
        # we inform the father that we are a sequence and our position
        if value is None:
            if self.lists[-1][2] is None:
                self.lists[-1][2] = [seq]
                position = -1
                return "seq", True, position
            else:
                return "seq", False, seq

        elif value[0] == "seq":
            # if our child is a sequence at the end of the sequence
            # then we add our self in his list and give our postion to our father, informing him
            # that we are a sequence at the end of the sentence 'False'
            # FATHER -> SEQ(us) -> SEQ -> epsilon
            if value[1]:
                position = value[2]
                self.lists[position][2].insert(0, seq)
                return "seq_list", position
            # if our child is not at the end, we create a list with our self and our child
            # and we pass it to our father with our position, telling him that we are a sequence
            # and not at the end of the sentence
            # FATHER -> SEQ (us) -> SEQ -> DONOTCARE
            else:
                prev_seq = value[2]
                seq_list = [seq, prev_seq]
                return "seq_seq", False, seq_list
        # son is a gene and if we are here he has no sequence linked to him
        elif value[0] == "gene":
            position = value[2]
            self.lists[position][2] = [seq]
            return None

        # FATHER -> SEQ (us) -> SEQ -> GENE
        elif value[0] == "seq_gene":
            position = value[1]
            self.lists[position][2].append(seq)
            return None

        elif value[0] == "seq_list":
            self.lists[value[1]][2].insert(0, seq)
            return "seq_list", value[1]

        else:
            sys.stderr.write("case non géré dans Gene2Seq" + str(value) + "\n")

    # Visit a parse tree produced by SentenceGrammarv2Parser#geneToGene.
    def visitGeneToGene(self, ctx):
        """
        visitor function for rule gene to gene
        :param ctx:
        :return: send information to the father according to the value send by his children and his context
        """
        value = self.visit(ctx.eg())
        gene = ctx.GENE().getText()

        if value is None:
            return 'gene', [gene]

        elif value[0] == "seq":
            if value[1]:
                position = value[2]
                self.lists[position][1] = gene
                return "gene_seq", position
                # sys.stderr.write("cas non géré toi même tu sais")

            else:
                self.lists.append([None, gene, [value[2]]])
                return None

        elif value[0] == "gene":
            gene_list = [gene] + value[1]
            return "gene_list", gene_list

        elif value[0] == "seq_list":
            return "seq_list_gene", [gene], value[1]

        elif value[0] == "seq_seq":
            seq_list = value[2]
            position = -1
            if self.lists[-1][2] is None:
                self.lists[-1][2] = seq_list
            else:
                self.lists.append([None, None, seq_list])
            return "seq_list_gene", [gene], position

        elif value[0] == "seq_list_gene":
            position = value[2]
            gene_list = [gene] + value[1]
            return "seq_list_gene", gene_list, position

        else:
            sys.stderr.write("case non géré dans GeneToGene" + str(value) + "\n")

    # Visit a parse tree produced by SentenceGrammarv2Parser#geneToTarg.
    def visitGeneToTarg(self, ctx):
        """
        visitor function for rule gene to targeting
        :param ctx:
        :return: send information to the father according to the value send by his children and his context
        """
        value = self.visit(ctx.et())
        targ = ctx.TARG().getText()

        if value is None:
            sys.stderr.write("cas non traité in G2T " + str(value) + "\n")

        elif value[0] == "seq":
            if value[1]:
                sys.stderr.write("cas non traité in G2T " + str(value) + "\n")
            else:
                position = -1
                seq = value[2]
                if self.lists[-1][2] is None and self.lists[-1][1] is None and self.lists[-1][0] is None:
                    self.lists[-1][0] = targ
                    self.lists[-1][2] = [seq]
                else:
                    self.lists.append([targ, None, [seq]])
                return "targ_seq", position

        elif value[0] == "seq_seq":
            position = value[1]
            self.lists[position][0] = targ
            return "targ_seq", position

        elif value[0] == "nt_seq_list":
            seq_list = value[1]
            last_seq = seq_list.pop()
            nt = value[2]
            if self.lists[-1][2] is None and self.lists[-1][1] is None:
                self.lists[-1][2] = [last_seq]
                self.lists[-1][0] = nt
            else:
                self.lists.append([nt, None, [last_seq]])
            return "targ_seq_list", seq_list, targ

        else:
            sys.stderr.write("cas non traité G2T" + str(value) + "\n")

    # Visit a parse tree produced by SentenceGrammarv2Parser#geneToNonTarg.
    def visitGeneToNonTarg(self, ctx):
        """
        visitor function for rule gene to targeting
        :param ctx:
        :return: send information to the father according to the value send by his children and his context
        """
        value = self.visit(ctx.ent())
        ntarg = ctx.NONTARG().getText()

        if value is None:
            sys.stderr.write("cas non traité dans G2NT" + str(value) + "\n")

        elif value[0] == "seq":
            if value[1]:
                self.lists[value[2]][0] = ntarg
            else:
                self.lists.append([ntarg, None, [value[2]]])
            return None

        elif value[0] == "seq_list":
            seq_list = self.lists[value[1]][2]
            self.lists[value[1]][2] = None
            last_seq = seq_list.pop()
            if self.lists[-1][2] is None and self.lists[-1][1] is None:
                self.lists[-1][2] = [last_seq]
                self.lists[-1][0] = ntarg
            else:
                self.lists.append([ntarg, None, [last_seq]])
            self.lists.append([None, None, seq_list])
            return "seq_list", -1

        else:
            sys.stderr.write("cas non traité dans G2NT" + str(value) + "\n")

        return self.visitChildren(ctx)

    # Visit a parse tree produced by SentenceGrammarv2Parser#geneFinish.
    def visitGeneFinish(self, ctx):
        """
        Rule corresponding to the end of the sentence after a gene
        :param ctx:
        :return: None to his father to indicate that it is the end of the sentence; epsilon
        """
        return None

    # Visit a parse tree produced by SentenceGrammarv2Parser#seq.
    def visitSeq(self, ctx):
        """
        visitor function for rule seq
        :param ctx:
        :return: send information to the father according to the value send by his children and his context
        """
        value = self.visit(ctx.esp())
        seq = ctx.SEQ().getText()

        if seq == "<missing <INVALID>>" or seq == "<missing SEQ>":
            sys.stderr.write("erreur lexer faut lever une exception")
            return None

        elif value is None:
            return 'seq', seq

        elif value[0] == "gene":
            if value[1]:
                self.lists[-1][1] = None
                return 'seq', seq

        elif value[0] == "seq":
            if value[1]:
                position = value[2]
                self.lists[position][2].append(seq)
                return "seq_list", position
            if not value[1]:
                self.lists.append([None, None, [value[2]]])
            return 'seq', seq

        elif value[0] == "seq_list":
            self.lists[value[1]][2].insert(0, seq)
            return "seq_list", value[1]

        else:
            sys.stderr.write("case non géré dans Seq" + str(value) + "\n")

    # Visit a parse tree produced by SentenceGrammarv2Parser#nontargToSeq.
    def visitNontargToSeq(self, ctx):
        """
        visitor function for rule non targeting to sequence
        :param ctx:
        :return: send information to the father according to the value send by his children and his context
        """
        value = self.visit(ctx.esp())
        seq = ctx.SEQ().getText()

        if value is None:
            if self.lists[-1][2] is None and self.lists[-1][0] is None and self.lists[-1][1] is None:
                self.lists[-1][2] = [seq]
                return "seq", True, -1
            else:
                return "seq", False, seq

        elif value[0] == "seq":
            if value[1]:
                position = value[2]
                self.lists[position][2].insert(0, seq)
                return "seq_list", position
            else:
                seq_before = value[2]
                position = -1
                if self.lists[-1][2] is None and self.lists[-1][1] is None:
                    self.lists[-1][2] = [seq, seq_before]
                else:
                    self.lists.append([None, None, [seq, seq_before]])
                return "seq_list", position

        else:
            sys.stderr.write("cas non traité NT2S" + str(value) + "\n")

    # Visit a parse tree produced by SentenceGrammarv2Parser#seqToTarg.
    def visitSeqToTarg(self, ctx):
        """
        visitor function for rule sequence to Targeting
        :param ctx:
        :return: send information to the father according to the value send by his children and his context
        """
        value = self.visit(ctx.et())
        targ = ctx.TARG().getText()

        if value is None:
            sys.stderr.write("faut lever une exception si on a une phrase du genre nt1 s1 t1")
            return None

        elif value[0] == "seq":
            position = value[1]
            self.lists[position][0] = targ
            return None

        elif value[0] == "gene":
            position = value[1]
            self.lists[position][0] = targ
            return "targ_gene", position

        elif value[0] == "gene_list":
            return "targ_gene_list", value[1], targ

        elif value[0] == "tar_gene_seq":
            self.lists[value[1]][0] = targ
            return None

        else:
            sys.stderr.write("case non géré dans Seq2Targ" + str(value) + "\n")

    # Visit a parse tree produced by SentenceGrammarv2Parser#seqToNontarg.
    def visitSeqToNontarg(self, ctx):
        """
        visitor function for rule sequence to non targeting
        :param ctx:
        :return: send information to the father according to the value send by his children and his context
        """
        value = self.visit(ctx.es())
        nontar = ctx.NONTARG().getText()
        if value is None:
            if self.lists[-1][0] is None and self.lists[-1][1] is None and self.lists[-1][2] is None:
                self.lists[-1][0] = 'nontargeting'
                return None

        elif value[0] == "seq":
            if self.lists[-1][2] is None:
                self.lists[-1][0] = nontar
                self.lists[-1][2] = [value[1]]
            else:
                self.lists.append([nontar, None, [value[1]]])
            return None

        else:
            sys.stderr.write("case non géré dans SeqToNontarg" + str(value) + "\n")

    # Visit a parse tree produced by SentenceGrammarv2Parser#seqToGene.
    def visitSeqToGene(self, ctx):
        """
        visitor function for rule sequence to gene
        :param ctx:
        :return: send information to the father according to the value send by his children and his context
        """
        value = self.visit(ctx.eg())
        gene = ctx.GENE().getText()

        # the gene is at the end of the sentence, we affect him in the first available list list[0]
        if value is None:
            if self.lists[-1][1] is None and self.lists[-1][0] is None:
                self.lists[-1][1] = gene
                position = -1
                return "gene", True, position
            else:
                position = -1
                self.lists.append([None, gene, None])
                return "gene", False, position

        if value[0] == "seq":
            if value[1]:
                position = value[2]
                self.lists[position][1] = gene
                # return "gene", False, position
                return None
            else:
                # position = -1
                self.lists.append([None, gene, [value[2]]])
                # return "gene", False, position
                return None

        elif value[0] == "seq_seq":
            if value[1]: # devenu seq_list
                position = value[2]
                self.lists[position][1] = gene
                return None
            else:
                seqs = value[2]
                self.lists.append([None, gene, seqs])
                return None

        elif value[0] == "seq_list":
            self.lists[value[1]][1] = gene
            return None

        elif value[0] == "seq_list_gene":
            seq_pos = value[2]
            nb_seq = len(self.lists[seq_pos][2])
            seq_list = self.lists[seq_pos][2]
            self.lists[seq_pos][2] = None

            gene_list = [gene] + value[1]
            nb_gene = len(gene_list)

            seq_p_gene = int(nb_seq / nb_gene)
            count = 0
            for gene in gene_list:
                if self.lists[-1][1] is None and self.lists[-1][0] != "nontargeting":
                    self.lists[-1][1] = gene
                    self.lists[-1][2] = seq_list[count:count + seq_p_gene]
                    count += seq_p_gene
                else:
                    self.lists.append([None, gene, seq_list[count:count + seq_p_gene]])
                    count += seq_p_gene
            return None

        elif value[0] == "gene" or "gene_list":
            gene_list = [gene] + value[1]
            return "gene_list", gene_list

        else:
            sys.stderr.write("case non géré dans Seq2Gene" + str(value) + "\n")

    # Visit a parse tree produced by SentenceGrammarv2Parser#seqToSeq.
    def visitSeqToSeq(self, ctx):
        """
        visitor function for rule sequence to sequence
        :param ctx:
        :return: send information to the father according to the value send by his children and his context
        """
        value = self.visit(ctx.esp())
        seq = ctx.SEQ().getText()

        # the next element is the Epsilon, we add the sequence to the first list (we know it is empty
        # because we are the first element
        # we send the value 'True' to the father to inform him
        # that we are the last element
        if value is None:
            if self.lists[-1][2] is None and self.lists[-1][0] is None:
                self.lists[-1][2] = [seq]
                position = -1
                return "seq", True, position
            else:
                return "seq", False, seq

        # the next element is gene
        elif value[0] == "gene":
            position = value[2]
            # if the gene does not already have a sequence, the current sequence
            # is linked to the gene (add to his sequence list)
            if self.lists[position][2] is None:
                self.lists[position][2] = [seq]
                return "seq_gene", position
            # if the gene already have a sequence we send the sequence to the father to deal with it
            # and we inform that we are not at the end of the sentence with 'False'
            else:
                return "seq", False, seq

        elif value[0] == "seq":
            if value[1]:
                position = value[2]
                self.lists[position][2].insert(0, seq)
                return "seq_list", position
            if not value[1]:
                seq_before = value[2]
                position = -1
                if self.lists[-1][2] is None and self.lists[-1][1] is None:
                    self.lists[-1][2] = [seq, seq_before]
                else:
                    self.lists.append([None, None, [seq, seq_before]])
                return "seq_list", position

        elif value[0] == "seq_list":
            self.lists[value[1]][2].insert(0, seq)
            return "seq_list", value[1]

        elif value[0] == "gene_list":
            gene_list = value[1]
            return "seq_gene_list", [seq], gene_list

        elif value[0] == "seq_gene":
            position = value[1]
            self.lists[position][2].append(seq)
            return None

        elif value[0] == "seq_gene_list":
            seq_list = [seq] + value[1]
            return "seq_gene_list", seq_list, value[2]

        elif value[0] == "targ_gene":
            position = value[1]
            self.lists[position][2] = [seq]
            return "seq_targ_gene", position

        elif value[0] == "seq_targ_gene":
            position = value[1]
            self.lists[position][2].append(seq)
            return "seq_list_targ_gene", position

        else:
            sys.stderr.write("case non géré dans Seq2Seq" + str(value) + "\n")

    # Visit a parse tree produced by SentenceGrammarv2Parser#seqFinish.
    def visitSeqFinish(self, ctx):
        """
        Rule corresponding to the end of the sentence after a sequence
        :param ctx:
        :return: None to his father to indicate that it is the end of the sentence; epsilon
        """
        return None
