grammar SentenceGrammarv2;

e: TARG et  # firstTarg
 | NONTARG es # firstNonTarg
 | GENE eg  # firstGene
 | SEQ esp  # firstSeq
 ;


et: SEQ esp # targToSeq
   | GENE eg # targToGene
   | NONTARG ent # targToNonTarg
   ;

eg: SEQ esp # geneToSeq
   | GENE eg # geneToGene
   | TARG et # geneToTarg
   | NONTARG ent # geneToNonTarg
   |    #geneFinish
   ;

es: SEQ esp #seq;

ent: SEQ esp # nontargToSeq
    ;

esp: TARG et  #seqToTarg
   | NONTARG es #seqToNontarg
   | GENE eg  #seqToGene
   | SEQ esp  #seqToSeq
   |     #seqFinish
   ;


SEQ : ('s'|'S')[0-9]+ ;
NONTARG : 'nontargeting'[0-9]+ ;
TARG : 'targeting'[0-9]+ ;
GENE : ('g'|'G')[0-9]+ ;

WS : [ \t\r\n]+ -> skip ;