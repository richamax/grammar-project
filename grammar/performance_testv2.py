import fileinput
import re
import collections
from grammar_exec import *
from sentence_transformator import *
from pipeline import nonblank_lines


def main():
    """
    Fonction qui prend en arguments:
    1) le fichier contenant les informations extraites par la grammaire
    2) le fichier contenant les phrases extraites par le pre processing
    3) le fichier reference Gold.txt
    4) dossier contenant les fichier de test
    :return: Le nombre de sequences total dans l'ensemble des articles, le nombre de sequence trouvé par le
    pre processing, le nombre de sequence traité par la grammaire, le pourcentage de status correctement accordé,
    le pourcentage de gene correctement assigné à la bonne séquence, le pourcentage de triplet contenant les bonnes
    information pour chaque séquence
    """
    if len(sys.argv) < 5:
        print(" Usage: python3 performance_testv2.py path_to_obtained_results path_to_sentence path_to_gold folder_test_file/*")
        return

    fichier_extracted = sys.argv[1]
    fichier_sentences = sys.argv[2]
    fichier_gold = sys.argv[3]

    total = 0 # total des sequences contenu dans l'ensemble des fichiers
    total_seq_find = 0 # total des sequences correctement extraite par les algorithm de pre traitement
    total_seq_tagged = 0 # total des sequences traité par la grammaire

    triple_juste = 0 # nombre de triplet ( seq status gene ) correctement extait

    status_juste = 0

    gene_juste = 0

    seen_file = {}

    # go through all the file present in the folder
    # if the file is in gold, we add it in the dictionary with all of the sentences of this file present in gold
    for present_file in sys.argv[4:]:
        file_id = present_file.split("/")[-1].split(".")[0]
        with open(fichier_gold, 'r') as gold:
            for line in nonblank_lines(gold):
                if line[0] != "#":
                    id = line.split("\t")[0]
                    sequence = line.split("\t")[1]
                    if id == file_id:
                        if id not in seen_file.keys():
                            seen_file[id] = []
                        seen_file[id].append(sequence)
                        total += 1

    with open(fichier_sentences, 'r') as f:
        for line in nonblank_lines(f):
            if line[0] != "#":
                line = line.split('.')[0]
                line = line.split(" ")
                file_id = line[0].split("_")[0]
                if file_id in seen_file:
                    for elem in line:
                        if re.match(r"[ATCGUatcgu]{8,}", elem):
                            total_seq_find += 1

    with open(fichier_extracted, 'r') as f:
        for l in nonblank_lines(f):
            if l[0] != "#":
                file_number, sentence_id, sequence, status, gene = l.split("\t")
                print("Currently treating sentence " + str(sentence_id))
                sys.stderr.write("Currently treating sentence " + str(sentence_id)+"\n")
                with open(fichier_gold, 'r') as gold:
                    for line in nonblank_lines(gold):
                        if line[0] != "#":
                            words = line.split("\t")[:4]
                            if words[0] == file_number and words[1] == sequence:
                                total_seq_tagged += 1
                                if status == words[2]:
                                    status_juste += 1
                                    if gene.upper() == words[3].upper() or (gene.upper() == "ACTB" and words[3].upper() == "ACTIN|ACTB"):
                                        gene_juste += 1
                                        triple_juste += 1
                                    else:
                                        sys.stderr.write("seq expected:" + str(words[1]) + " found: " + str(sequence) + "\n")
                                        sys.stderr.write("status expected: " + str(words[2]) + " found: " + str(status) + "\n")
                                        sys.stderr.write("gene expected: " + str(words[3]) + " found: " + str(gene) + "\n")
                                else:
                                    if gene.upper() == words[3].upper() or (gene.upper() == "ACTB" and words[3].upper() == "ACTIN|ACTB"):
                                        gene_juste += 1
                                    sys.stderr.write("seq :" + str(words[1]) + " " + sequence + "\n")
                                    sys.stderr.write("status: " + str(words[2]) + " " + status + "\n")
                                    sys.stderr.write("gene: " + str(words[3]) + " " + gene + "\n")
                                break

    folder = str(sys.argv[4]).split("/")[-2]
    # with open("result.txt", 'a') as res:
    #     res.write("The total number of sequences present in all of the files of " + folder + " is : " + str(total) + "\n")
    #     res.write("The total number of extracted sequences by the pre processing algorithm are : " + str(total_seq_find) + "\n")
    #     res.write("The total number of sequences treated by the grammar is : " + str(total_seq_tagged) + "\n")
    #     res.write("The total number of correct status attribution by the grammar is : " + str(status_juste) + " which correspond to : " + str((status_juste/total_seq_find)*100) + " percent\n")
    #     res.write("The total number of correct gene attribution by the grammar is : " + str(gene_juste) + " which correspond to : " + str((gene_juste/total_seq_find)*100) + " percent\n")
    #     res.write("The total number of correct triplet attribution by the grammar is : " + str(triple_juste) + " which correspond to : " + str((triple_juste/total_seq_find)*100) + " percent\n")
    print("The total number of sequences present in all of the files of " + folder + " is : " + str(total))
    print("The total number of extracted sequences by the pre processing algorithm are : " + str(total_seq_find))
    print("The total number of sequences treated by the grammar is : " + str(total_seq_tagged))
    print("The total number of correct status attribution by the grammar is : " + str(status_juste) + " which correspond to : " + str((status_juste/total_seq_find)*100) + " percent")
    print("The total number of correct gene attribution by the grammar is : " + str(gene_juste) + " which correspond to : " + str((gene_juste/total_seq_find)*100) + " percent")
    print("The total number of correct triplet attribution by the grammar is : " + str(triple_juste) + " which correspond to : " + str((triple_juste/total_seq_find)*100) + " percent")




if __name__ == "__main__":
    # execute only if run as a script
    main()