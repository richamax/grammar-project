import sys
import re
from bs4 import BeautifulSoup
from pipeline import nonblank_lines

not_end = ["Inc.", "Ltd.", "inc.", "ltd.", "Co.", "co.", "no."]


def get_text_bs(html):
    tree = BeautifulSoup(html, 'lxml')

    body = tree.body
    if body is None:
        return None

    for tag in body.select('script'):
        tag.decompose()
    for tag in body.select('style'):
        tag.decompose()

    text = body.get_text(separator='\n')
    return text


def create_phrases(text):
    phrases = []
    sentence = ""
    end_w = True
    for line in nonblank_lines(text):
        all_words = re.split(" |-|\t", line)
        for word in all_words:
            longeur = str(len(word))
            if len(word) > 1:
                selected = re.findall(r"[ATCGUatcgu]{" + longeur + ",}", word)
            else:
                selected = None
            # if the word is composed of only letter suc as ATCGUatcgu we continue
            if selected:
                # we retrive the postion of the word in the sentence
                position_word = all_words.index(word)
                extended_word = word
                # we iterate over the remaning words
                while position_word != len(all_words) - 1:
                    next_word = all_words[position_word + 1]
                    longeur_cutted = str(len(next_word))
                    cut = re.findall(r"[ATCGUatcgu]{" + longeur_cutted + ",}", next_word)
                    # we check if the next word is also composed of letter such as ATCGUatcgu
                    # If it is the case that mean the ARN sequence has been cutted, so we merge them
                    # and we delet the second part of the sequence
                    # if it is not the case we stop the loop
                    if cut:
                        del all_words[position_word + 1]
                        extended_word += cut[0]
                    else:
                        break
                # v2
                all_words[all_words.index(word)] = extended_word
                word = extended_word

            sentence += word + " "

        all_words = re.split(" |\n", sentence)

        sentence = ""
        for word in all_words:
            if word == "reaction":
                a = 10
            if "." in word:
                for spe_word in not_end:
                    if spe_word in word and end_w:
                        sentence += word + " "
                        end_w = False
                if word[-1] != ".":
                    sentence += word + " "
                    end_w = False
                if end_w:
                    sentence += word
                    phrases.append(sentence)
                    sentence = ""
                end_w = True
            elif word != "":
                sentence += word + " "

    return phrases


def main():
    """
    Program to test the extraction of sentences from an html file
    :return:
    """
    # wrong usage
    if len(sys.argv) < 2:
        print("Usage : ./sentence_extractor folder_containing_html_to_process/*")
        return

    # define the name of the output files
    gold_sequences = "extracted_sequences_html.txt"
    gold_sentences = "extracted_sentences_html.txt"
    corp_name = 'test'

    # formatage des fichiers
    with open(gold_sequences, "a") as f:
        f.write("###########\n## " + corp_name + "\n###########\n\n")
        f.write("#PMID\tSID\tSEQ\tSTATUS\tGENE\n\n")
    with open(gold_sentences, "a") as f:
        f.write("###########\n## " + corp_name + "\n###########\n\n")

    # we go through every file of the folder
    fichier = sys.argv[2]
    if ".html" in fichier:
        #retreive the ID of the PDF file
        number = fichier.split("/")[-1].split(".")[0]
        file_name = number+".pdf"

        print("Extracting DNA and RNA sequences form " + file_name + "\n")

        #id of the current sentence
        sid = 1

        first_opening = True
        current_file = open(fichier, "rb")
        text = get_text_bs(current_file)

        # break into lines and remove leading and trailing space on each
        lines = (line.strip() for line in text.splitlines())
        # break multi-headlines into a line each
        chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
        # drop blank lines
        text = '\n'.join(chunk for chunk in chunks if chunk)

        text = text.split("\t")
        for elem in text:
            if elem == "\n":
                elem.replace("\n", " ")
            elif elem == "":
                elem.replace("", " ")

        phrases = create_phrases(text)
        nb_phrase = 0
        for phrase in phrases:
            nb_phrase += 1
            words = re.split(' |\(|\)|-|,|!|\?|;|:|\.|‐|"', phrase)
            list_word = []
            # we iterate over all the word of the sentence
            for word in words:

                if re.match(r"[ATCGUatcgu]{8,}", word):
                    list_word.append(word)

            if list_word:
                with open(gold_sentences, "a") as f_sen:
                    if first_opening:
                        f_sen.write("####################\n")
                        f_sen.write("#" + file_name + "\n")
                        f_sen.write("####################\n\n")
                    # v1
                    # f_sen.write(number+"_S"+str(sid)+"\t"+phrase+"\n\n")
                    f_sen.write(number + "_S" + str(sid) + "\t")
                    for elem in words:
                        f_sen.write(" " + elem)
                    f_sen.write(".\n\n")
                with open(gold_sequences, "a") as result:
                    if first_opening:
                        result.write("#" + file_name + "\n\n")
                        first_opening = False
                    for elem in list_word:
                        result.write(number + "\t" + number + "_S" + str(sid) + "\t" + elem + "\n")
                    result.write("\n")
                sid += 1




if __name__ == "__main__":
    # execute only if run as a script
    main()