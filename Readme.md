The depot contains 2 main folders. The usage for all of the scripts are detailed in each files and will be summed up
below.

The following instructions are for ubuntu users.

**If you moved the emplacement of the files "Gold.txt" or "extracted_sentences_html.txt" and so on simply replace the path in the 
different scripts that you are using.**

## Grammar

The folder Grammar, contains all of the files necessary to execute the pipeline and the evaluation of the obtained
result.
In order to have the grammar running you need to have ANTLR4 install on your computer
In this folder you will find 4 other folders:

+ error_output: The folder contains two files in which the error messages of both pipeline and
performance_testv2 algorithms are written.

If you want to add any modification to the grammar file you will need to recompile it using the following command:

`$> antlr4 -Dlanguage=Python3 -visitor -no-listener -o grammar_tools SentenceGrammarv2.g4`

+ grammar_tools: Contains the different files produces by ANTLR while compiling the grammar. These files are need to
use the grammar.

+ html_files: Contains the files we want to process.

+ pipeline_output: Contains the output produce by the pipeline.

The folder grammar also contains the following python scripts:

+ input.txt: file containing a tokenized sentence to be process by the grammar.

+ grammar_exec.py: Given an input in "input.txt"", produce list containing the triple of information the grammar was
able to extract from the intput.

`$> python3 grammar_exec.py input.txt`

+ MyVisitorv2.py: This is a personalize tree visitor, allowing us for each rule of the grammar to have a specific
treatment of the input.

+ pipeline.py: This script will extract the text from all of the html file of a given folder and will extract the
information from the text of the different articles and save them in files called: 

    + extracted_information_html.txt: contain the list of triplet for each sentence we found with nucleotide sequences.
    + exacte_sentences_html.txt: all the sentences we found with nucleotide sequences.
    
both stored in pipeline_output

`$> python3 pipeline.py corpus_m ../SeekAndBlastn/Listes/Gold.txt html_files/*`

+ performance_test_v2.py: This is the evaluation script, is compared the obtained results with the expected one.

`$> python3 performance_testv2.py pipeline_output/extracted_information_html.txt pipeline_output/extracted_sentences_html.txt ../SeekAndBlastn/Listes/Gold.txt html_files/* `

+ script_pipeline_test.sh: Clean the old results and launch the pipeline and the evaluation test python script.

`$> ./script_pipeline_test.sh`

+ sentence_extractor_html.py: This script is used to test the extraction of the text from a html file.

`$> python3 sentence_extractor.py path_to_th_file_to_process`

+ sentence_transformator.py: Given a sentence, return a string with the keywords tokenized.

`$> python3 sentence_transformator.py "sentence to process"`

+ SentenceGrammarv2.g4: File containing the different rules of the grammar.


------------------------------------------------------------------------------------------------------------------------

## Neural_file_creator

The folder neural_file_creator, contains the python script that generator the input and the expected output for
the training set of the neural network.
To launch the script:

`$> python3 neural_files_creator.py ../grammar/pipeline_output/extracted_sentences_html.txt ../SeekAndBlastn/Listes/Gold.txt`

