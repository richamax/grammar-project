import sys
sys.path.append('../grammar')

from grammar_exec import *
from sentence_transformator import *
from pipeline import nonblank_lines, is_special_gene


def create_gene_file(sentences_file):
    """
    Function that create a file containing the name of all the genes detected in all of the sentences
    present in the input file
    :param sentences_file: file containing sentences from which we want to extract the genes
    :return:
    """
    with open("genes_in_sentences.txt", "w") as full:
        with open(sentences_file, "r") as file:
            for line in nonblank_lines(file):
                if line[0] != "#":
                    words = line.split(" ")

                    for elem in words:
                        if elem == "":
                            words.remove(elem)

                    found_genes = []
                    position = 0
                    for current_word in words:
                        found_genes, found = is_special_gene(current_word, position, found_genes, words)

                        if not found:
                            if is_gene(current_word):
                                found_genes.append(current_word)
                        position += 1

                    if not found_genes:
                        full.write("none")

                    else:
                        for g in found_genes[:-1]:
                            full.write(str(g) + " ")
                        full.write(str(found_genes[-1]))

                    full.write("\n")


def create_sequences_file(sentences_file):
    """
    Function that create a file containing all the sequences detected in all of the sentences present in the input file
    :param sentences_file: file containing sentences from which we want to extract the sequences
    :return:
    """
    with open("sequences_in_sentences.txt", "w") as full:
        with open(sentences_file, "r") as file:
            for line in nonblank_lines(file):
                if line[0] != "#":
                    list_seq = []
                    sentence_id, sentence = line.split("\t")
                    file_number = sentence_id.split("_")[0]
                    words = sentence.split(" ")

                    for elem in words:
                        if elem == "":
                            words.remove(elem)

                    for elem in words:
                        if is_seq(elem):
                            list_seq.append(elem)

                    if not list_seq:
                        full.write("none")

                    else:
                        for s in list_seq[:-1]:
                            full.write(str(s) + " ")
                        full.write(str(list_seq[-1]))
                    full.write("\n")


def create_triplet_file(gold_file, sentences_file):
    """
    Function that create a file containing all of the information we were able to extract from the sentences.
    It will create for each sentences a list of triplet containing information about the status of the sequence,
    the gene and the sequence
    [ [ sequence , status , gene ] ...]
    :param gold_file: reference file, containing for each article the information claimed in the text
    :param sentences_file: file containing sentences from which we want to extract the information about the sequences
    :return:
    """
    with open("triple_in_sentences.txt", "w") as full:
        with open(sentences_file, "r") as file:
            for line in nonblank_lines(file):
                if line[0] != "#":
                    triple = []
                    sentence_id, sentence = line.split("\t")
                    file_number = sentence_id.split("_")[0]
                    words = sentence.split(" ")

                    for elem in words:
                        if elem == "":
                            words.remove(elem)

                    for elem in words:
                        if is_seq(elem):
                            with open(gold_file, "r") as gold:
                                for l in nonblank_lines(gold):
                                    if l[0] != "#":
                                        id = l.split("\t")[0].split(" ")[0]
                                        if id == file_number:
                                            sequence, status, gene = l.split("\t")[1:4]
                                            if sequence == elem:
                                                triple.append([elem, status, gene])

                    if not triple:
                        full.write("none")

                    else:
                        full.write("[ ")
                        for sub_l in triple:
                            full.write("[ ")
                            for elem in sub_l[:-1]:
                                full.write(elem + " , ")
                            full.write(elem + " ")
                            full.write("]")
                        full.write(" ]")
                    full.write("\n")


def create_first_level_input(sentences_file):
    """
    This function go through all of the sentences present in the file sentence_file, and get rid of the useless space.
    In addition it also detect when some type of genes has been cut during the pre-processing of the sentences, in the
    pipeline algorithm and restore them i.e: if a gene such as miR-127 has been cut into miR 127 it will be remerge as
    miR-127
    :param sentences_file: file containing the sentence that were extracted by the pipeline algorithm
    :return:
    """
    with open("full_sentences.txt", "w") as full:
        with open(sentences_file, "r") as file:
            for line in nonblank_lines(file):
                if line[0] != "#":
                    sentence_id, sentence = line.split("\t")
                    file_number = sentence_id.split("_")[0]

                    new_sentence = ""

                    words = sentence.split(" ")
                    for elem in words:
                        if elem == "":
                            words.remove(elem)
                    gene = []

                    for pos, elem in enumerate(words):
                        gene, found = is_special_gene(elem, pos, gene, words)
                        if found:
                            words[pos] = gene[0]
                            del words[pos+1]
                        gene = []

                    for elem in words:
                        new_sentence += elem + " "

                    full.write(new_sentence + "\n")


def create_second_level_input(sentences_file):
    """
    Transform the sentences present in sentence_file and replace the words that are not of interest by the token "NA"
    the gene are replaced by "gene", the sequences sur as AGCTCGU are replaced by "sequence" and the word corresponding
    to the status are replace by "targeting" or "nontargeting" depending on the status represented by the word
    :param sentences_file: file produce by the create_first_level_input function
    :return:
    """
    with open("sentences_tokenized_no_number.txt", "w") as full:

        with open(sentences_file, "r") as file:
            for line in nonblank_lines(file):
                if line[0] != "#":
                    sentence = line.split("\n")[0]
                    words = sentence.split(" ")
                    for elem in words:
                        if elem == "":
                            words.remove(elem)

                    transformed_sentence = ""
                    previous_word = ""

                    counter = 0
                    for current_word in words:
                        value_nt, nb_nt = is_not_targeting(previous_word, current_word)
                        value_t, nb_t = is_targeting(previous_word, current_word)

                        if current_word == "control":
                            position = counter
                            if position < len(words) - 1:
                                if words[position - 1]:
                                    previous_word = words[position - 1]
                                else:
                                    previous_word = ""

                                if words[position + 1]:
                                    next_word = words[position + 1]
                                else:
                                    next_word = ""
                                seq = previous_word + " " + current_word
                                if seq not in nottargeting_list_double and seq not in targeting_list_double and next_word != "non" and next_word != "negative" and next_word != "." and previous_word not in nottargeting_list and next_word not in nottargeting_list:
                                    transformed_sentence += "non-targeting "

                        if is_gene(current_word):
                            transformed_sentence += "gene "

                        elif is_seq(current_word):
                            transformed_sentence += "sequence "

                        elif value_nt:
                            if nb_nt == 1:
                                transformed_sentence += "non-targeting "
                            else:
                                transformed_sentence += "nontargeting nontargeting "

                        elif value_t:
                            if nb_t == 1:
                                transformed_sentence += "targeting "
                            else:
                                transformed_sentence += "targeting targeting "
                        else:
                            transformed_sentence += "NA "

                        previous_word = current_word
                        counter += 1

                    full.write(transformed_sentence + "\n")


def create_third_level_input(sentences_file):
    """
    Transform the sentences present in sentence_file and replace the words that are not of interest by the token "NA"
    the gene are replaced by "gene", the sequences sur as AGCTCGU are replaced by "sequence" and the word corresponding
    to the status are replace by "targeting" or "nontargeting" depending on the status represented by the word.
    In addition, the number of gene/status/sequences is take into account, for example the third gene will be replace
    by "gene3"
    :param sentences_file: file produce by the create_first_level_input function
    :return:
    """
    with open("sentences_tokenized_indexed.txt", "w") as full:
        with open(sentences_file, "r") as file:
            for line in nonblank_lines(file):
                if line[0] != "#":
                    sentence = line.split("\n")[0]
                    words = sentence.split(" ")
                    for elem in words:
                        if elem == "":
                            words.remove(elem)

                    transformed_sentence = ""
                    previous_word = ""
                    nb_gene = 0
                    nb_seq = 0
                    nb_nottarg = 0
                    nb_targ = 0
                    counter = 0
                    for current_word in words:
                        value_nt, nb_nt = is_not_targeting(previous_word, current_word)
                        value_t, nb_t = is_targeting(previous_word, current_word)

                        if current_word == "control":
                            position = counter
                            if position < len(words) - 1:
                                if words[position - 1]:
                                    previous_word = words[position - 1]
                                else:
                                    previous_word = ""

                                if words[position + 1]:
                                    next_word = words[position + 1]
                                else:
                                    next_word = ""
                                seq = previous_word + " " + current_word
                                if seq not in nottargeting_list_double and seq not in targeting_list_double and next_word != "non" and next_word != "negative" and next_word != "." and previous_word not in nottargeting_list and next_word not in nottargeting_list:
                                    nb_nottarg += 1
                                    transformed_sentence += "non-targeting"+str(nb_nottarg)+" "

                        if is_gene(current_word):
                            nb_gene += 1
                            transformed_sentence += "gene"+str(nb_gene)+" "

                        elif is_seq(current_word):
                            nb_seq += 1
                            transformed_sentence += "sequence"+str(nb_seq)+" "

                        elif value_nt:
                            if nb_nt == 1:
                                nb_nottarg += 1
                                transformed_sentence += "non-targeting"+str(nb_nottarg)+" "
                            else:
                                nb_nottarg += 1
                                transformed_sentence += "non-targeting"+str(nb_nottarg)+" non-targeting"+str(nb_nottarg)+" "

                        elif value_t:
                            if nb_t == 1:
                                nb_targ += 1
                                transformed_sentence += "targeting"+str(nb_targ)+" "
                            else:
                                comb = previous_word + " " + current_word
                                nb_targ += 1
                                transformed_sentence += "targeting"+str(nb_targ)+" targeting"+str(nb_targ)+" "
                        else:
                            transformed_sentence += "NA "

                        previous_word = current_word
                        counter += 1

                    full.write(transformed_sentence + "\n")


def main():
    """
    Produce the different file necessary to train the neural network
    :return:
    """
    if len(sys.argv) < 3:
        print("Usage : python3 neural_files_creator.py path_to_file_containing_the_extracted_sentences path_to_gold_file")
        return

    sentences_file = sys.argv[1]
    gold_file = sys.argv[2]

    # Create a dictionary of human genes contained in the "Homo_sapiens.gene_info.txt" file
    fill_gene_dic("../SeekAndBlastn/Listes/Homo_sapiens.gene_info.txt")

    # Create the file "full_sentences.txt", containing the sentence cleaned up.
    create_first_level_input(sentences_file) # good

    # Create the file "sentences_tokenized_no_number.txt", the keyword such as gene, sequence and status have been
    # replace by token, the other words have been replace by NA.
    create_second_level_input("full_sentences.txt") # good

    # Create the file "sentences_tokenized_indexed.txt", same as the previous one but here the keyword are indexed.
    create_third_level_input("full_sentences.txt") # good

    # Create the file "genes_in_sentences.txt", contain for each sentence the name of the genes present in the sentence.
    create_gene_file(sentences_file) # some issues to detect some genes

    # Create the file "sequences_in_sentences.txt", contain for each sentence, the different nucleotides sequences.
    create_sequences_file(sentences_file) # good

    # Create the file "triple_in_sentences.txt", contain for each sentence, the triple telling us if a sequence is
    # targeting or not, and if it is for which gene.
    create_triplet_file(gold_file, sentences_file) # not good erreur dans le gold et extraction


if __name__ == "__main__":
    # execute only if run as a script
    main()
