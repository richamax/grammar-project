Med Oncol (2012) 29:311–317
DOI 10.1007/s12032-010-9808-5

ORIGINAL PAPER

RNAi-mediated downregulation of NOB1 suppresses the growth
and colony-formation ability of human ovarian cancer cells
Yang Lin • Shuai Peng • Hansong Yu
Hong Teng • Manhua Cui

•

Received: 12 October 2010 / Accepted: 27 December 2010 / Published online: 2 February 2011
Ó Springer Science+Business Media, LLC 2011

Abstract Nin one binding protein (NOB1p), encoded by
the NOB1 gene, is a crucial molecule in the maturation of
the 20S proteasome and protein degradation. The present
study evaluates whether NOB1 is an appropriate molecular
target for cancer gene therapy. In two ovarian cancer cell
lines, SKOV3 and HEY, NOB1 expression was knocked
down by a lentiviral short hairpin RNA (shRNA) delivery
system. The RNA interference (RNAi)-mediated the
downregulation of NOB1 expression markedly reduced the
proliferative and colony-formation ability of ovarian cancer cells. Additionally, NOB1 shRNA-expressing lentivirus-treated ovarian cancer cells tended to arrest in the G0/
G1 phase. These results suggested that NOB1 may act as an
oncogenic factor in ovarian cancer and could be a potential
molecular target for ovarian cancer gene therapy.
Keywords shRNA  NOB1  Ovarian cancer  Lentivirus 
Cellular proliferation

Introduction
Protein degradation is an important process tightly regulated by a diverse group of proteases. [1]. The degradation

Yang Lin and Shuai Peng contributed equally to this work.
Y. Lin  H. Teng  M. Cui (&)
Department of Gynaecology and Obstetrics, The Second
Hospital of Jilin University, No. 257 Ziqiang Street, Nanguan
District, 130041 Changchun, Jilin Province, China
e-mail: manhuacui@163.com
S. Peng  H. Yu
College of Food Science and Engineering, Jilin Agricultural
University, Jilin, China

of a protein via the ubiquitin proteasome pathway (UPP)
involves two successive steps: (1) the covalent linkage
of multiple ubiquitin molecules to the substrate and (2)
the degradation of the polyubiquitinated protein by the
26S proteasome complex with the release of free and
reusable ubiquitin [2]. The 26S proteasome is a biological
macromolecule consisting of two parts: the 19S regulatory particle (RP or PA700), which confers ATP dependency and ubiquitinated substrate specificity on the
enzyme, and the 20S proteasome (CP), which forms the
proteolytic core [3]. Numerous studies have shown that
the ubiquitin (Ub) pathway plays a critical role in regulating essential cellular processes, such as gene transcription and signal transduction [4]. The Ub pathway
also takes part in the modulation of protein turnover in
the cell cycle; thus, this process is commonly mutated
and is involved in cancer development, especially in
malignant tumors [3, 5].
NOB1 was first identified in Saccharomyces cerevisiae
as an essential gene encoding the Nin one binding protein
(NOB1p), which can interact with Rpn12p, as demonstrated previously by a two-hybrid assay [6]. The nuclear
protein NOB1p serves as a chaperone to join the 20S
proteasome with the 19S regulatory particle in the nucleus
and facilitates the maturation of the 20S proteasome.
Therefore, the function of NOB1p is necessary for UPPmediated proteolysis [7]. A recent study in chronic myeloid
leukemia (CML) reveals that NOB1, along with five other
genes, can be used as a diagnostic marker discriminating
chronic phase (CP) from blast crisis (BC) CML [8]. Also,
our previous investigation on ovarian cancer indicated that
the expression levels of NOB1 in ovarian cancer tissues
were remarkably higher in contrast to those of controls
(data not shown), indicating that NOB1 may act as an
oncogenic factor in ovarian cancer.

123

312

To test our hypothesis, we applied RNA interference
(RNAi) technology to knock down the expression of NOB1
in two ovarian cancer cell lines, and we then investigated
the proliferation, cell cycle and colony-formation capacity
in both cell lines. Our data revealed that the inhibition of
NOB1 significantly decreased proliferation in both cell
lines, providing us with a future target for therapy.

Materials and methods
Lentiviral vector production
Small interfering RNA (siRNA) targeting NOB1 sequence
(AAGGTTAAGGTGAGCTCATCG) and non-silencing
sequence (AATTCTCCGAACGTGTCACGT) were transformed into short hairpin RNA (shRNA) (stem–loop–stem
structure) and were cloned into pLV-THM-lentiviral vectors with BamHI/EcoRI sites. Then, the recombined pLVTHM-lentiviral vector and two-helper vector system
(GeneChem Co. LTD., Shanghai, China) were transfected
into HEK293T cells via Lipofectamine 2000 (Invitrogen,
Carlsbad, CA, USA) to generate lentivirus. After 3 days of
incubation, the lentivirus from culture medium was collected and concentrated with Centricon-plus-20 (Millipore,
Billerica, MA, USA).
Cell culture and infection
SKOV3 and HEY cells were received from the American
Type Culture Collection (ATCC). Cells were grown in
DMEM (Invitrogen, Carlsbad, CA, USA) containing 10%
fetal bovine serum (FBS; Invitrogen, Carlsbad, CA, USA),
2 mM L-glutamine and 1% penicillin/streptomycin at 37°C
with 5% CO2. For lentivirus infection, SKOV3 and
HEY cells were cultured in 6-well plates. Then, NOB1
shRNA-expressing lentivirus (sh-NOB1) or nontargeting
shRNA-expressing lentivirus (control) was added, with a
multiplicity of infection (MOI) of 10 in SKOV3 cells and
20 in HEY cells. After 72 h of infection, cells were
observed under fluorescence microscopy (MicroPublisher
3.3RTV; Olympus, Tokyo, Japan).

Med Oncol (2012) 29:311–317

Beacon Designer 7 software (Premier Biosoft International,
Palo Alto, CA, USA) as follows: Actin-F, 50 -CGGCATTG
TCACCAACTG-30 , Actin-R, 50 -CGCTCGGTCAGGATCT
TC-30 ; NOB1-F, 50 -ATCTGCCCTACAAGCCTAAAC-30 ,
NOB1-R, 5-TCCTCCTCCTCCTCCTCAC-30 . The SYBR
Green Real-Time PCR assay kit (TAKARA, Otsu, Japan)
was used, and quantitative real-time PCR (qRT-PCR) was
performed according to the ABI manufacturer’s protocols
(Perkin Elmer Corp./Applied Biosystems, Foster City, CA,
USA). Fluorescence was analyzed by using the Light
Cycler Software version 3.5 (Roche Diagnostics, Meylan,
France). All samples were examined in triplicate.
Western blot analysis
Total protein was isolated from whole cells using ice-cold
protein lysis buffer (1% Triton X-100; 50 mM Tris–HCl,
pH 7.4; 150 mM NaCl; 0.1% SDS; 1 mM PMSF; 1 mM
EDTA). This was followed by 30 min of incubation on ice
and centrifugation at 10,000 9 g for 10 min at 4°C. Protein concentration was determined by BCA protein assay
(Pierce, Rockford, IL, USA). Protein extracts were separated on a SDS-polyacrylamide gel, blotted onto a nitrocellulose membrane and incubated with anti-Nob1p
antibody (Abcam, Cambridge, UK) or anti-Actin antibody
(Santa Cruz, CA, USA). Western blotting was developed
using horseradish peroxidase-conjugated goat anti-mouse
or goat anti-rabbit IgG (Santa Cruz Biotechnology, Santa
Cruz, CA, USA) and was detected with enhanced chemiluminescence reagent (Santa Cruz Biotechnology, Santa
Cruz, CA, USA).
Methylthiazoletetrazolium cell proliferation assay
Cells infected with NOB1 shRNA lentivirus (sh-NOB1) or
non-silencing shRNA lentivirus (control), along with nontreated cells, were seeded in a 96-well plates at a density of
2,000 cells per well. At indicated time points, 20 lL
methylthiazoletetrazolium (MTT) solution (5 mg/mL) was
added into each well. After 4 h of incubation at 37°C,
150 lL dimethyl sulfoxide (DMSO) was added to dissolve
the crystals. After 10 min at room temperature, the absorbance was recorded at 570 nm.

Quantitative real-time PCR
BrdU incorporation assay
SKOV3 and HEY cells were cultured in 6-well plates and
were then infected with lentivirus for 72 h. Total RNA was
isolated from cultured cells by Trizol reagent (Invitrogen,
Carlsbad, CA, USA). cDNA was synthesized from total
RNA with random primers following the manufacturer’s
protocol (MBI Fermantas, Vilnius, Lithuania). Two sets of
primers were used for PCR. Primers were designed by

123

Cells infected with sh-NOB1 or control, along with nontreated cells, were cultured in 96-well plates with 2,000
cells per well. A 5-bromodeoxyuridine (BrdU) incorporation assay was performed by using the BrdU Cell Proliferation Assay kit (Chemicon, Temecula, CA, USA).
Briefly, 20 lL of 1/500 diluted BrdU was added, and the

Med Oncol (2012) 29:311–317

assay was incubated for 8 h. Then, 100 lL of 1/200 diluted
anti-BrdU and peroxidase-conjugated goat anti-mouse IgG
antibodies were used successively, according the manufacturer’s instructions. The plates were washed, and then
100 lL TMB Peroxidase Substrate was added. Plates were
read at a dual wavelength of 450/550 nm, and the growth
rate of cells was calculated by the following equation:
Growth rate ¼ ðOD48h  OD24h Þ=OD24h :
FACS analysis
Cells were cultured in 6-well plates and were then treated
with sh-NOB1 or control. At the indicated time point, cells
were collected by centrifugation at 2000 9 g for 5 min,
were washed twice with PBS, and were fixed in ethanol.
Then, cells were rehydrated and resuspended in PBS containing RNase-A (100 lg/mL) on ice. After an additional
incubation at room temperature for 30 min, cells were
stained with propidium iodide (PI) and were then analyzed
by BD FACS Calibur Flow Cytometer (BD Biosciences,
San Diego, CA, USA).
Colony-formation assay
Three groups of cells (sh-NOB1, control and non-infected
cells) were plated in 6-well plates at a concentration of 200
cells per well. Cells were allowed to grow for 14 days to
form colonies. At the indicated time point, cells were
washed twice with PBS, treated with Giemsa for 10 min,
washed three times with ddH2O, and then photographed
with a digital camera. The number of colonies ([50 cells/
colony) were counted under fluorescence microscopy
(MicroPublisher 3.3RTV; Olympus, Tokyo, Japan).

313

in Fig. 1a, the infection efficiency of lentivirus was greater
than 80% after 72 h of infection. The qRT-PCR assay
suggested that NOB1 mRNA level was reduced by about
70% in both cell lines treated with NOB1 shRNA lentivirus, as compared with the control group (Fig. 1b). We also
determined the level of NOB1p protein in cells after 72 h
of lentivirus infection via western blot analysis. In SKOV3
and HEY cell lines, the protein expression of NOB1p was
significantly reduced by about 50% through NOB1 shRNA
lentivirus treatment (Fig. 1c).
NOB1 is important for ovarian cancer cell growth
To further assess the role of NOB1 in regulating ovarian
cancer cell proliferation, MTT assays were performed on
both SKOV3 and HEY cells following lentivirus infection
for 72 h. Figure 2a shows that there were no statistically
significant differences in viability between control cells
and non-infected cells, indicating that the lentiviral system
itself had no cytotoxic effect on cells, whereas the viability
of HEY cells was markedly inhibited by NOB1 knockdown
(P \ 0.05 compared to control). In another ovarian cancer
cell line, SKOV3, the inhibitory effect of sh-NOB1 on cell
proliferation can be observed beginning on day 2; it
became more obvious on days 4 and 5 (P \ 0.05 compared
with the control). Moreover, BrdU incorporation assays
also revealed that the inhibition of NOB1 expression significantly reduced the growth rate of SKOV3 and HEY
ovarian cancer cells during the 48-h incubation period
(P \ 0.05 compared with the control, Fig. 2b). These
findings sustained the notion that the knockdown of NOB1
greatly diminished cell proliferative ability.
Knockdown of NOB1 repressed cell colony formation

Statistical analysis
The data shown are presented as the mean ± standard
deviation (SD) of three independent experiments. Statistical
significance was determined with Student’s t test. A P value
of less than 0.05 was considered significant.

Results
Knockdown of NOB1 by shRNA lentivirus system
in ovarian cancer cells
To investigate the role of NOB1 in ovarian cancer, shRNA
targeting NOB1 or non-silencing sequences were cloned
into pLV-THM-lentiviral vector, respectively. Then, NOB1
shRNA lentivirus or non-silencing shRNA lentivirus
expressing GFP were generated and infected into two
ovarian cancer cell lines, SKOV3 and HEY cells. As shown

We then studied the colony-formation capacity of SKOV3
cells treated by NOB1 shRNA lentivirus. HEY cells were
not used in this experiment because they could only form
small colonies, as compared with SKOV-3 cells. Three
groups of SKOV3 cells (sh-NOB1, control and non-infected cells) were allowed to grow for 14 days to form colonies. As shown in Fig. 3a and b, NOB1 knockdown resulted
in a nearly 0.3-fold decrease in the number of colonies, as
compared with the two control groups (P \ 0.01), whereas
no obvious difference in the number of colonies was found
between control lentivirus-infected cells and non-infected
cells.

Inhibition of NOB1 induced G0/G1 arrest
Knowing that the inhibition of NOB1 in both SKOV3 and
HEY cells markedly slows cell proliferation, we further

123

314

Med Oncol (2012) 29:311–317

Fig. 1 NOB1 silencing efficiency by shRNA lentivirus. a Lentivirus
infection in ovarian cancer cell lines. Fluorescence photomicrographs
of ovarian cells infected by lentivirus. Pictures were taken 72 h after
infection at a magnification of 9100. b Identification of NOB1
knockdown efficiency using shRNA lentivirus by real-time PCR in
SKOV3 and HEY cells. c Identification of NOB1 knockdown

efficiency using shRNA lentivirus by western blot analysis. Reduced
NOB1p protein levels in SKOV3 and HEY cells are shown. Actin was
used as a loading control. (control: non-silencing shRNA lentivirus;
sh-NOB1: NOB1 shRNA lentivirus). * P \ 0.05 compared with the
control

Fig. 2 NOB1 is important for ovarian cancer cell proliferation.
a NOB1 silencing by shRNA lentivirus resulted in growth inhibition
as detected by MTT assay in SKOV3 and HEY cells. Cells infected
with NOB1 shRNA lentivirus (sh-NOB1) or non-silencing shRNA
lentivirus (control) for 72 h, along with non-treated cells, were seeded

in a 96-well plates, and cell viability was determined at indicated time
points. b NOB1 silencing by shRNA lentivirus led to ovarian cell
growth inhibition as detected by a BrdU incorporation assay. The
growth rates in SKOV3 and HEY cells were calculated. All assays
were performed in triplicate. * P \ 0.05 compared with the control

employed cell-cycle analysis to uncover the mechanism
governing the inhibitory effect of sh-NOB1 on cell proliferation. As shown in Fig. 4, in SKOV3 cells, an obvious
increase in G1-phase cell population (P \ 0.01) was
observed in the sh-NOB1 group accompanied by a slight

decrease in the S-phase cell population, as compared with
the two control groups. Moreover, HEY cells were slow to
progress through the cell cycle, having a marked G0/G1
phase delay. Our results revealed that sh-NOB1 exerted an
inhibitory effect on ovarian cell proliferation via G0/G1

123

Med Oncol (2012) 29:311–317

315

cell-cycle arrest, indicating that NOB1 may promote cancer
cell growth.

Discussion

Fig. 3 NOB1 silencing repressed ovarian cancer cell colony formation. a Photomicrographs of Giemsa-stained colonies of SKOV3 cells
growing in 6-well plates for 14 days after infection. b The number of
cells in each colony of SKOV3 cells was counted. Cell number in shNOB1 group was significantly reduced, as compared with the control
group (* P \ 0.05)

The cellular proteome is in a dynamic state consisting of
synthesis and degradation. Degradation of extracellular
proteins is mainly mediated non-specifically by the lysosomes or released proteases, while the proteolysis of
intracellular protein, including nuclear proteins, is catalyzed by the ubiquitin–proteasome pathway [9]. NOB1p is
a nuclear protein involved in protein degradation and
controlled proteolysis. NOB1p regulates the maturation of
the 20S proteasome and is then degraded to complete 26S
proteasome biogenesis [7]. In light of the important role of
NOB1 and the critical function of ubiquitin-dependent
proteolysis in universal biological processes, we assumed
that NOB1 may influence oncogenesis through UPP-mediated protein degradation.
A number of studies have underscored the link between
UPP and cancer. Decades ago, a number of oncogene and
suppressor gene products were found to be targets of
ubiquitination. For example, nuclear oncoproteins, such as
c-myc, c-fos, p53 and E1A, are among the most rapidly
degraded intracellular proteins. In vitro studies show that

Fig. 4 NOB1 silencing induced a G0/G1 arrest in ovarian cancer
cells. a FACS histograms and cell-cycle analysis of SKOV3 and HEY
cells following non-silencing shRNA or NOB1 shRNA lentivirus
infection. b Quantification of the percentage of cells in cell-cycle

phases G1, S, and G2/M. In the sh-NOB1 group, an obvious increase
in the G1-phase cell population and a significant decrease in the
S-phase cell population was observed, as compared with the control
group (* P \ 0.05)

123

316

the ubiquitin system can mediate the degradation of these
oncoproteins [10–12]. Also, a number of oncogenic
mutations and suppressor gene disruptions have been
shown to affect ubiquitination and proteasomal degradation
[5]. In colorectal cancer, mutations at several points of the
b-catenin gene disrupt its ubiquitination and degradation,
leading to the accumulation of b-catenin in the cells [13].
Moreover, a clinical study investigating mantle cell lymphoma (MCL) shows that MCLs have normal p27Kip-1
mRNA expression but increased p27Kip-1 protein degradation activity via the proteasome pathway, which is
associated with a decreased overall survival in patients
[14]. Therefore, the ubiquitin system may mediate the
degradation of cancer-related genes that are important in
tumorigenesis. However, the specific functions of the
proteins in UPP, especially the roles of NOB1 in ovarian
cancer, are still unclear.
In this study, we presumed that NOB1 may act as an
oncogenic factor in ovarian cancer development. Given the
prevalence and availability of RNAi technology in cancer
research or cancer therapy [15], we used a lentivirus
shRNA system that can effectively knock down the
expression of NOB1 at both the RNA and protein levels. As
shown in Fig. 1, qRT-PCR and western blot analysis
showed sufficient silencing of NOB1, thus ensuring the
credibility of the subsequent assays. Predictably, reduced
expression of NOB1 in both ovarian cell lines greatly
decreased cancer cell proliferation, as confirmed by MTT
and BrdU cell proliferation assays (Fig. 2). We also proved
that knockdown of NOB1 notably inhibited the colonyformation capacity of SKOV3 cells (Fig. 3). However,
HEY cells did not easily form colonies in this assay, as
reported previously [16]; thus, they were omitted. Together, these results indicate that NOB1 is important for
ovarian cancer cell growth in the short or relative long
term. We then applied a cell-cycle analysis by FACS,
attempting to uncover the mechanism by which sh-NOB1
controls ovarian cell growth. Intriguingly, our data reveal
that sh-NOB1 had an inhibitory effect on ovarian cancer
cell growth via G0/G1 arrest (Fig. 4).
In addition, the present study provided new evidence
pertinent to the role and function of UPP-related proteins in
cancer research. In this study, however, we did not discover
exactly how NOB1 influences cancer cell proliferation. We
inferred that the dysregulation of NOB1 may affect the Ub
pathway and degradation of certain proteins that govern
cell cycling, such as p27Kip-1 or other cell-cycle regulators.
For example, the levels of p27Kip-1 are largely controlled
by post-transcriptional ubiquitin-mediated degradation
[17]. Therefore, aberrant regulation of NOB1 may eventually influence the levels of p27Kip-1 and may disrupt cell
growth control. To test this hypothesis, further research

123

Med Oncol (2012) 29:311–317

examining the interaction between NOB1p and its downstream target proteins is needed.

Conclusion
The present study proved for the first time that RNAimediated knockdown of NOB1 suppresses the growth and
colony-formation ability of ovarian cancer cells. In addition, NOB1 inhibition arrests the cell cycle in the G0/G1
phase. Our data indicated that NOB1 may serve as an
oncogene in ovarian cancer development. Therefore, NOB1
has considerable potential to be a new therapeutic target for
the treatment of ovarian cancer.
Acknowledgments The authors are thankful for financial support
from the National Natural Science Foundation of China (30973187)
and Jilin Science and Technology Funds (200705203, 20080134 and
200905146).

References
1. Simpson MV. The release of labeled amino acids from the proteins of rat liver slices. J Biol Chem. 1953;201:143–54.
2. Glickman MH, Ciechanover A. The ubiquitin-proteasome proteolytic pathway: destruction for the sake of construction. Physiol
Rev. 2002;82:373–428.
3. Ferrell K, Wilkinson CR, Dubiel W, Gordon C. Regulatory
subunit interactions of the 26S proteasome, a complex problem.
Trends Biochem Sci. 2000;25:83–8.
4. Hershko A, Ciechanover A. The ubiquitin system. Annu Rev
Biochem. 1998;67:425–79.
5. Mani A, Gelmann EP. The ubiquitin-proteasome pathway and its
role in cancer. J Clin Oncol. 2005;23:4776–89.
6. Tone Y, et al. Nob1p, a new essential protein, associates with the
26S proteasome of growing saccharomyces cerevisiae cells.
Gene. 2000;243:37–45.
7. Tone Y, Toh EA. Nob1p is required for biogenesis of the 26S
proteasome and degraded upon its maturation in Saccharomyces
cerevisiae. Genes Dev. 2002;16:3142–57.
8. Oehler VG, et al. The derivation of diagnostic markers of chronic
myeloid leukemia progression from microarray data. Blood.
2009;114:3292–8.
9. Bader N, Jung T, Grune T. The proteasome and its role in nuclear
protein maintenance. Exp Gerontol. 2007;42:864–70.
10. Rogers S, Wells R, Rechsteiner M. Amino acid sequences common to rapidly degraded proteins: the PEST hypothesis. Science.
1986;234:364–8.
11. Ciechanover A, Finley D, Varshavsky A. Ubiquitin dependence
of selective protein degradation demonstrated in the mammalian
cell cycle mutant ts85. Cell. 1984;37:57–66.
12. Ciechanover A, et al. Degradation of nuclear oncoproteins by the
ubiquitin system in vitro. Proc Natl Acad Sci USA.
1991;88:139–43.
13. Sparks AB, Morin PJ, Vogelstein B, Kinzler KW. Mutational
analysis of the APC/beta-catenin/Tcf pathway in colorectal cancer. Cancer Res. 1998;58:1130–4.
14. Chiarle R, et al. Increased proteasome degradation of cyclindependent kinase inhibitor p27 is associated with a decreased

Med Oncol (2012) 29:311–317
overall survival in mantle cell lymphoma. Blood. 2000;95:
619–26.
15. Izquierdo M. Short interfering RNAs as a tool for cancer gene
therapy. Cancer Gene Ther. 2005;12:217–27.
16. Xu Y, Fang XJ, Casey G, Mills GB. Lysophospholipids activate
ovarian and breast cancer cells. Biochem J 1995;309:933–40.

317
17. Shirane M, et al. Down-regulation of p27(Kip1) by two mechanisms, ubiquitin-mediated degradation and proteolytic processing. J Biol Chem. 1999;274:13886–93.

123

