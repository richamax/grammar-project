#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 10 02:36:09 2020

@author: utente
"""
#%% For CMD

import sys
import nltk
nltk.download('punkt')
from nltk import sent_tokenize

file_name = sys.argv[1]
f = open(file_name)
data = f.read()
f.close()

par3 = sent_tokenize(data)
tokenized_sent_after_remove_n = [x.replace('\n',' ') for x in par3]


#%% For Spyder

#from nltk import sent_tokenize
#para1 =  open("text.txt")
#para = para1.read()
#
##paragraphs = [p for p.replace('\n', ' ') in para.split('.') if p]
#par3 = sent_tokenize(para)
#tokenized_sent_after_remove_n = [x.replace('\n',' ') for x in par3]

#%%
# import importany libraries
import torch
import numpy as np
from transformers import AutoTokenizer

tokenizer = AutoTokenizer.from_pretrained("monologg/biobert_v1.0_pubmed_pmc")
tag_values = ['O','I-PRGE','B-PRGE']

#%%
# Load model
# Model class must be defined somewhere
PATH_bio = 'my_bio_bert_model_bc2gm'

new_model = torch.load(PATH_bio)
#new_model.eval()

#%%
items = []
for i in tokenized_sent_after_remove_n:
    items.append(i)
    
input_blocks = [x[:-1] for x in items]

#%%

Result = []

for i in input_blocks:
    
    input_sentences = i
    
    #print(input_sentences)
    
    tokenized_sentence = tokenizer.encode(input_sentences)
    
    
    input_ids = torch.tensor([tokenized_sentence]).cuda()
    
    
    with torch.no_grad():
        output = new_model(input_ids)
    label_indices = np.argmax(output[0].to('cpu').numpy(), axis=2)
    
    
    # join bpe split tokens
    tokens = tokenizer.convert_ids_to_tokens(input_ids.to('cpu').numpy()[0])
    new_tokens, new_labels = [], []
    for token, label_idx in zip(tokens, label_indices[0]):
        if token.startswith("##"):
            new_tokens[-1] = new_tokens[-1] + token[2:]
        else:
            new_labels.append(tag_values[label_idx])
            new_tokens.append(token)
     
           
    #for token, label in zip(new_tokens, new_labels):
        #print("{}\t{}".format(label, token)) 
    
    result = list(map(list, zip(new_tokens, new_labels))) 
    
    Result.append(result)
    
#%% Save to csv

import numpy as np  
import pandas as pd 
pd.DataFrame(np.array(Result)).to_csv("biobert_bc2gm_results.txt", header=None, index=None)   

#%%


print('PROCESS SUCCESSFUL')
"""""""""""""""""""""""""""""""""""""""""   END   """""""""""""""""""""""""""""""""""""""""""""""""""""""""""




#%%
#%%
#para1 =  open("text.txt")
#text = para1.read()
#
#text_split = text.split()
#
#def batch_list(ns, batch_size):
#    return [ns[i:i+batch_size] for i in range(0, len(ns), batch_size)]
#
#input_blocks = batch_list(text_split, 200) 

#%%
#%%




