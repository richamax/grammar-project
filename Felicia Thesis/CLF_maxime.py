#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 15 11:36:58 2020

@author: utente
"""

#%% For CMD

import sys
import nltk
nltk.download('punkt')
from nltk import sent_tokenize

file_name = sys.argv[1]
f = open(file_name)
data = f.read()
f.close()

par3 = sent_tokenize(data)
tokenized_sent_after_remove_n = [x.replace('\n',' ') for x in par3]


#%% For Spyder

from nltk import sent_tokenize
para1 =  open("text.txt")
para = para1.read()

#paragraphs = [p for p.replace('\n', ' ') in para.split('.') if p]
par3 = sent_tokenize(para)
tokenized_sent_after_remove_n = [x.replace('\n',' ') for x in par3]


#%%
import pickle

filename_maxime = 'finalized_CLF_model_maxime.sav'
#filename_bc2gm = 'finalized_CLF_model_bc2gm.sav'


#%%
def TokenExtractor(text):
    item = []    
    for q in text: 
        if '-' in q or '_' in q: 
            for i in range(len(q)): 
                if q[i] == "-": 
                    j = i + 1 
                    item.append(q[:i]) 
                    item.append("-") 
                    item.append(q[j:]) 
                elif q[i] == "_": 
                    j = i + 1 
                    item.append(q[:i]) 
                    item.append("_") 
                    item.append(q[j:])
        else: 
            item.append(q) 
    return item

#%%

items = []
for i in tokenized_sent_after_remove_n:
    items.append(i)

from nltk.tokenize import  RegexpTokenizer
tokenizer =  RegexpTokenizer('\s+', gaps=True)
text1 = [x[:-1] for x in items]
t_w1 = [TokenExtractor(TokenExtractor((tokenizer.tokenize(word)))) for word in text1] 

def remove_space(list1):
    empty1 = []
    for string in list1:
        if string != '' :
            empty1.append(string)
    return empty1
            
t_w2 = [remove_space(x) for x in t_w1]      
    

#%%
def word2features(sentence, index):
    """ sentence: [w1, w2, ...], index: the index of the word """
    return {
        'word': sentence[index],
        'is_first': index == 0,
        'is_last': index == len(sentence) - 1,
        'is_capitalized': sentence[index][0].upper() == sentence[index][0],
        'is_all_caps': sentence[index].upper() == sentence[index],
        'is_all_lower': sentence[index].lower() == sentence[index],
        'prefix-1': sentence[index][0],
        'prefix-2': sentence[index][:2],
        'prefix-3': sentence[index][:3],
        'suffix-1': sentence[index][-1],
        'suffix-2': sentence[index][-2:],
        'suffix-3': sentence[index][-3:],
        'prev_word': '' if index == 0 else sentence[index - 1],
        'next_word': '' if index == len(sentence) - 1 else sentence[index + 1],
        'has_hyphen': '-' in sentence[index],
        'has_underscore': '_' in sentence[index],
        'is_numeric': sentence[index].isdigit(),
        'capitals_inside': sentence[index][1:].lower() != sentence[index][1:]
    }

#%% load the model from disk

loaded_model = pickle.load(open(filename_maxime, 'rb'))


#%%
def transform_to_dataset(tagged):
    X= []
    for index in range(len(tagged)):
        X.append(word2features(tagged, index))
    return X

def merge(list1, list2): 
    merged_list = [(list1[i]["word"], list2[i]) for i in range(len(list2))] 
    return merged_list 

def gene_tag(sentence):
    tags = loaded_model.predict(sentence)
    tag=(sentence,tags)
    return merge(tag[0], tag[1])

#%% Predict for every sentence
    
Result = []
    
for i in t_w2:
    
    X = transform_to_dataset(i)
        
    output = gene_tag(X)
        
    #print(gene_tag(X))
    
    Result.append(output)

#%% Save to text

import numpy as np  
import pandas as pd 
pd.DataFrame(np.array(Result)).to_csv("CLF_maxime_results.txt", header=None, index=None)   

#%%      

print('PROCESS SUCCESSFUL')

"""""""""""""""""""""""""""""""""""""""""""  END """""""""""""""""""""""""""""""""""""""""""""""""""      


#%% 
    
